﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace CotizadorAutos
{
    public partial class RegistroUsuario : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnChecar_Click(object sender, EventArgs e)
        {
            int ChecaUsuario = 0;

            try
            {
                 ChecaUsuario = AccesoDatos.fnVerificaUsuario(txtUsuario.Value);
            }

            catch(Exception x)
            {
                fnMensaje("error en verifica usuario " + x.Message.ToString());
            }            

            if(ChecaUsuario == 1)
            {
                tablaContraseña.Visible = true;
            }

            else if (ChecaUsuario == 2)
            {
                fnMensaje("Usuario registrado previamente");
                return;
            }

            else
            {
                fnMensaje("Usuario inexistente o baja");
                txtUsuario.Value = string.Empty;
                return;
            }
        }      

        protected void fnMensaje(string Mensaje)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + Mensaje + "');", true);
        }

        protected void btnRegistrar_Click(object sender, EventArgs e)
        {          

            if (txtConfirmaContrasenia.Text != txtContrasenia.Text)
            {
                fnMensaje("Las contraseñas no son iguales");
                txtConfirmaContrasenia.Text = "";
                txtContrasenia.Text = "";
                return;
            }

            if (txtConfirmaContrasenia.Text == string.Empty|| txtContrasenia.Text== string.Empty || txtUsuario.Value==string.Empty)
            {
                fnMensaje("Los campos no pueden estar vacios");
                return;
            }

            var registro = AccesoDatos.fnInsertaConstraseña(txtUsuario.Value, txtContrasenia.Text);

            if(registro == 1)
            {
                fnMensaje("Registro exitoso");
                Response.Redirect("~/DatosCliente.aspx");
            }
        }

        protected void btnEntrar_Click(object sender, EventArgs e)
        {
            if(txtUsuario2.Text == string.Empty || txtContraseña2.Text == string.Empty)
            {
                fnMensaje("Los campos no pueden estar vacios");
            }

            else
            {
                DataSet login = AccesoDatos.fnLogin(txtUsuario2.Text, txtContraseña2.Text);
                if (login.Tables[0].Rows[0]["id"].ToString() == "Usuario o contraseña incorrecta")
                {
                    fnMensaje(login.Tables[0].Rows[0]["id"].ToString());
                    return;
                }
                fnMensaje("hola " + login.Tables[0].Rows[0]["sNombre"].ToString());
                Session["NombreAgente"] = login.Tables[0].Rows[0]["sNombre"].ToString();
                Session["idUsuario"] = login.Tables[0].Rows[0]["id"].ToString();
                Session["CorreoAgente"] = login.Tables[0].Rows[0]["sCorreo"].ToString();

                var miMaster = (MasterPage)this.Master;
                ((Label)miMaster.FindControl("lblNombreAgente")).Text = Session["NombreAgente"].ToString();

                txtUsuario2.Text = string.Empty; 
                txtContraseña2.Text = string.Empty;
                Response.Redirect("DatosCliente.aspx");
            }
        }      
    }
}