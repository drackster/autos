﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master1.Master" AutoEventWireup="true" CodeBehind="Contrata.aspx.cs" Inherits="CotizadorAutos.Contrata" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div style="font-family:Calibri; font-size:large">
    <div style="text-align:center">
        <img src="Images/PLECA-AZUL.png" />
    </div>
    <div style="margin-left:400px">
        <asp:Label ID="Label1" runat="server" Text="Datos complementarios del contratante"></asp:Label>
    </div>
    <br />
    <div style="float:left; margin-left:400px">
    
               <div>
            <table style="text-align:right">
                <tbody>
                    <tr>
                        <td style="width:144px">
                            Nombre:
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                        </td>
                    
                    </tr>
                </tbody>
            </table>  
           </div>     
               <div>
            <table style="text-align:right">
                <tbody>
                    <tr>
                        <td style="width:144px">
                            Apellido Paterno:
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                      
                        </td>
                    </tr>
                </tbody>
            </table>  
           </div>     
               <div>
            <table style="text-align:right">
                <tbody>
                    <tr>
                        <td style="width:144px">
                            Apellido Materno
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>                           
                        </td>
                    </tr>
                </tbody>
            </table>  
           </div>     
               <div>
            <table style="text-align:right">
                <tbody>
                    <tr>
                        <td style="width:144px">
                            RFC:
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
                         </td>
                    </tr>
                </tbody>
            </table>  
           </div>             
     </div>

     <div style="float:right; margin-right:400px"">
    
               <div>
            <table style="text-align:right">
                <tbody>
                    <tr>
                        <td style="width:144px">
                            Edad:
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
                        </td>
                    
                    </tr>
                </tbody>
            </table>  
           </div>     
               <div>
            <table style="text-align:right">
                <tbody>
                    <tr>
                        <td style="width:144px">
                            Sexo:
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox6" runat="server"></asp:TextBox>
                      
                        </td>
                    </tr>
                </tbody>
            </table>  
           </div>     
               <div>
            <table style="text-align:right">
                <tbody>
                    <tr>
                        <td style="width:144px">
                            CURP:
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox7" runat="server"></asp:TextBox>                           
                        </td>
                    </tr>
                </tbody>
            </table>  
           </div>     
               <div>
            <table style="text-align:right">
                <tbody>
                    <tr>
                        <td style="width:144px">
                            Nacionalidad:
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox8" runat="server"></asp:TextBox>
                         </td>
                    </tr>
                </tbody>
            </table>  
           </div>             
     </div>
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    
     <div style="text-align:center">
        <img src="Images/PLECA-AZUL.png" />
    </div>
    <div style="margin-left:400px">
        <asp:Label ID="Label2" runat="server" Text="Datos adicionales del vehículo"></asp:Label>
    </div>
    <br />
    <div style="text-align:center; margin-left:700px">
         <table style="text-align:right">
                <tbody>
                    <tr>
                        <td style="width:144px">
                            Marca:
                        </td>
                        <td>
                            Marca
                         </td>
                    </tr>
                </tbody>
            </table>  

         <table style="text-align:right">
                <tbody>
                    <tr>
                        <td style="width:144px">
                            Submarca:
                        </td>
                        <td>
                            Submarca
                         </td>
                    </tr>
                </tbody>
            </table>  

         <table style="text-align:right">
                <tbody>
                    <tr>
                        <td style="width:144px">
                            Versión:
                        </td>
                        <td>
                            Version
                         </td>
                    </tr>
                </tbody>
            </table> 
        
         <table style="text-align:right">
                <tbody>
                    <tr>
                        <td style="width:144px">
                            Modelo:
                        </td>
                        <td>
                            2010
                         </td>
                    </tr>
                </tbody>
            </table>  
         
         <table style="text-align:right">
                <tbody>
                    <tr>
                        <td style="width:144px">
                            Numero de serie:
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox9" runat="server"></asp:TextBox>
                         </td>
                    </tr>
                </tbody>
            </table> 
        
         <table style="text-align:right">
                <tbody>
                    <tr>
                        <td style="width:144px">
                            Placa:
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox10" runat="server"></asp:TextBox>
                         </td>
                    </tr>
                </tbody>
            </table>   

         <table style="text-align:right">
                <tbody>
                    <tr>
                        <td style="width:144px">
                            Numero de motor:
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox11" runat="server"></asp:TextBox>
                         </td>
                    </tr>
                </tbody>
            </table>  

    </div>
</div>



</asp:Content>