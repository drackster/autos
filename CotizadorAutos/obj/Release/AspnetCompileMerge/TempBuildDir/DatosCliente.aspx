﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master1.Master" AutoEventWireup="True" CodeBehind="DatosCliente.aspx.cs" Inherits="CotizadorAutos.DatosCliente" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

       <div>

        

        <div>
            <asp:RadioButton ID="rbAutos" runat="server" Checked="true" Visible="false" />            
            <asp:RadioButton ID="rbCamiones" runat="server" Checked="false" Visible="false" />
        </div>
        <br />
        <br />
        <div id="barraAuto" style="width:auto; height: auto; margin-bottom: 0; text-align:center; font-family:Calibri">
                 <img alt="" class="auto-style1" src="Images/PLECA-AZUL.png" style="width:828px; height:8px" />
        </div>
        <br />
           
                <div id="formulairoAuto" style="width: 450px; height: auto; margin-bottom: 0; text-align:center; margin-left:450px; font-family:Calibri" >
	

                    <div style="width: 440px; height: 25px; float: left; line-height: 25px; margin-bottom: 10px;">
                        <div style="width: 140px; height: 25px; float: left; line-height: 25px; text-align: right;">
                            <label style="color: #003399;">*</label>
                            <label>Marca:</label>
                        </div>
                        <div style="width: 260px; height: 25px; float: left; margin-left: 25px; line-height: 25px;">
                            <div style="width:260px;">
                                <table summary="combobox" style="border-width:0;border-collapse:collapse;" class="">
                                    <tbody>
                                        <tr>
                                            <td style="width:100%;">
                                                <asp:DropDownList CssClass="ddl" ID="ddlMarca" runat="server" Width="250px" OnSelectedIndexChanged="ddlMarca_SelectedIndexChanged" AutoPostBack="true" ViewStateMode="Enabled" ></asp:DropDownList>
                                            </td> 
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div> 
                    
                     <div style="width: 440px; height: 25px; float: left; line-height: 25px; margin-bottom: 10px;">
                        <div style="width: 140px; height: 25px; float: left; line-height: 25px; text-align: right;">
                            <label style="color: #003399;">*</label>
                            <label>SubMarca:</label>
                        </div>
                        <div style="width: 260px; height: 25px; float: left; margin-left: 25px; line-height: 25px;">
                            <div style="width:260px;">
                                <table summary="combobox" style="border-width:0;border-collapse:collapse;" class="">
                                    <tbody>
                                        <tr>
                                            <td style="width:100%;">
                                                <asp:DropDownList CssClass="ddl" ID="ddlSubmarca" runat="server" Width="250px" Enabled="false" OnSelectedIndexChanged="ddlSubmarca_SelectedIndexChanged" AutoPostBack="true" ViewStateMode="Enabled"  ></asp:DropDownList>
                                            </td> 
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>    
                    
                     <div style="width: 440px; height: 25px; float: left; line-height: 25px; margin-bottom: 10px;">
                        <div style="width: 140px; height: 25px; float: left; line-height: 25px; text-align: right;">
                            <label style="color: #003399;">*</label>
                            <label>Año:</label>
                        </div>
                        <div style="width: 260px; height: 25px; float: left; margin-left: 25px; line-height: 25px;">
                            <div id="ctl00_ContentPlaceHolder1_rdcMarca"style="width:260px;">
                                <table summary="combobox" style="border-width:0;border-collapse:collapse;" class="">
                                    <tbody>
                                        <tr>
                                            <td style="width:100%;">
                                                <asp:DropDownList CssClass="ddl" ID="ddlaño" runat="server" Width="250px" OnSelectedIndexChanged="ddlaño_SelectedIndexChanged" AutoPostBack="true" ViewStateMode="Enabled" ></asp:DropDownList>
                                            </td> 
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>    
                    
                     <div style="width: 440px; height: 25px; float: left; line-height: 25px; margin-bottom: 10px;">
                        <div style="width: 140px; height: 25px; float: left; line-height: 25px; text-align: right;">
                            <label style="color: #003399;">*</label>
                            <label>Descripción:</label>
                        </div>
                        <div style="width: 260px; height: 25px; float: left; margin-left: 25px; line-height: 25px;">
                            <div style="width:260px;">
                                <table summary="combobox" style="border-width:0;border-collapse:collapse;" class="">
                                    <tbody>
                                        <tr>
                                            <td style="width:100%;">
                                                <asp:DropDownList CssClass="ddl" ID="ddldescripcion" runat="server" Width="250px" OnSelectedIndexChanged="ddldescripcion_SelectedIndexChanged" AutoPostBack="true" ViewStateMode="Enabled" ></asp:DropDownList>
                                            </td> 
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div> 

                    
              </div>
        <br />
      <div id="barraUbicacion" style="width:auto; height: auto; margin-bottom: 0; text-align:center; font-family:Calibri" >
                

          <img alt="" class="auto-style1" src="Images/PLECA-AZUL.png" style="width:828px; height:8px" /></div>
        <br />
      <div id="formulairoUbicacion" style="width: 450px; height: auto; margin-bottom: 0; text-align:center; margin-left:450px; font-family:Calibri" >

          <div style="width: 440px; height: 25px; float: left; line-height: 25px; margin-bottom: 10px;">
          <div style="width: 140px; height: 25px; float: left; line-height: 25px; text-align: right;">
                            <label style="color: #003399;">*</label>
                            <label>Código Postal:</label>
           </div>


          <div style="width: 260px; height: 25px; float: left; margin-left: 25px; line-height: 25px;">
           <div style="width:260px;">
                                <table summary="combobox" style="border-width:0;border-collapse:collapse;" class="">
                                    <tbody>
                                        <tr>
                                            <td style="width:100%;">
                                                <asp:TextBox ID="txtCP" runat="server" Width="250px" OnTextChanged="txtCP_TextChanged" AutoPostBack ="true" CssClass="ddl" MaxLength="5" ></asp:TextBox>
                                                
                                            </td> 
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
              </div>
      </div>

          <div id="divEstado" runat="server"  style="width: 440px; height: 25px; float: left; line-height: 25px; margin-bottom: 10px;" visible="false">
           <div style="width: 140px; height: 25px; float: left; line-height: 25px; text-align: right;">
                            <label style="color: #003399;">*</label>
                            <label>Estado:</label>
           </div>

          <div style="width: 260px; height: 25px; float: left; margin-left: 25px; line-height: 25px;">
           <div style="width:260px;">
                                <table summary="combobox" style="border-width:0;border-collapse:collapse;" class="">
                                    <tbody>
                                        <tr>
                                            <td style="width:100%;">
                                                <asp:DropDownList CssClass="ddl" ID="ddlEstado" runat="server" Width="250px" OnSelectedIndexChanged="ddlEstado_SelectedIndexChanged" AutoPostBack="true" ViewStateMode="Enabled" ></asp:DropDownList>
                                            </td> 
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
              </div>
              </div>

          <div id="divMunicipio" runat="server"  style="width: 440px; height: 25px; float: left; line-height: 25px; margin-bottom: 10px;" visible="false">
          <div style="width: 140px; height: 25px; float: left; line-height: 25px; text-align: right;">
                            <label style="color: #003399;">*</label>
                            <label>Municipio:</label>
           </div>

          <div style="width: 260px; height: 25px; float: left; margin-left: 25px; line-height: 25px;">
                 <div style="width:260px;">
                                <table summary="combobox" style="border-width:0;border-collapse:collapse;" class="">
                                    <tbody>
                                        <tr>
                                            <td style="width:100%;">
                                                <asp:DropDownList CssClass="ddl" ID="ddlMunicipio" runat="server" Width="250px" AutoPostBack="true" ViewStateMode="Enabled" OnSelectedIndexChanged="ddlMunicipio_SelectedIndexChanged" ></asp:DropDownList>
                                            </td> 
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                   </div>
              </div>
          <div id="divColonia" runat="server" style="width: 440px; height: 25px; float: left; line-height: 25px; margin-bottom: 10px;" visible="false">
          <div style="width: 140px; height: 25px; float: left; line-height: 25px; text-align: right;">
                            <label style="color: #003399;">*</label>
                            <label>Colonia:</label>
           </div>

          <div style="width: 260px; height: 25px; float: left; margin-left: 25px; line-height: 25px;">
           <div style="width:260px;">
                                <table summary="combobox" style="border-width:0;border-collapse:collapse;" class="">
                                    <tbody>
                                        <tr>
                                            <td style="width:100%;">
                                                <asp:DropDownList CssClass="ddl" ID="ddlColonias" runat="server" Width="250px" OnSelectedIndexChanged="ddlColonias_SelectedIndexChanged" AutoPostBack="true" ></asp:DropDownList>
                                            </td> 
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
              </div>
              </div>
          
          </div>
        <br />

        <div id="barraCliente" style="width:auto; height: auto; margin-bottom: 0; text-align:center; font-family:Calibri" >
                

          <img alt="" class="auto-style1" src="Images/PLECA-AZUL.png" style="width:828px; height:8px" /></div>
        <br />

          <div id="formulairoCliente" style="width: 450px; height: auto; margin-bottom: 0; text-align:center; margin-left:450px; font-family:Calibri" >

           <div style="width: 440px; height: 25px; float: left; line-height: 25px; margin-bottom: 10px;">
          <div style="width: 140px; height: 25px; float: left; line-height: 25px; text-align: right;">
                            <label style="color: #003399;">*</label>
                            <label>Nombre:</label>
           </div>



          <div style="width: 260px; height: 25px; float: left; margin-left: 25px; line-height: 25px;">
           <div style="width:260px;">
                                <table summary="combobox" style="border-width:0;border-collapse:collapse;" class="">
                                    <tbody>
                                        <tr>
                                            <td style="width:100%;">
                                                <asp:TextBox runat="server" Width="250px" ID="txtNombre" CssClass="ddl"></asp:TextBox>
                                            </td> 
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
              </div>
      </div>

           <div style="width: 440px; height: 25px; float: left; line-height: 25px; margin-bottom: 10px;">
          <div style="width: 140px; height: 25px; float: left; line-height: 25px; text-align: right;">
                            <label style="color: #003399;">*</label>
                            <label>Apellido:</label>
           </div>

          <div style="width: 260px; height: 25px; float: left; margin-left: 25px; line-height: 25px;">
           <div style="width:260px;">
                                <table summary="combobox" style="border-width:0;border-collapse:collapse;" class="">
                                    <tbody>
                                        <tr>
                                            <td style="width:100%;">
                                                <asp:TextBox runat="server" Width="250px" ID="txtApellido" CssClass="ddl"></asp:TextBox>
                                            </td> 
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
              </div>
      </div>
               <div style="width: 440px; height: 25px; float: left; line-height: 25px; margin-bottom: 10px;">
          <div style="width: 140px; height: 25px; float: left; line-height: 25px; text-align: right;">
                            <label style="color: #003399;">*</label>
                            <label>Edad:</label>
           </div>

          <div style="width: 260px; height: 25px; float: left; margin-left: 25px; line-height: 25px;">
           <div style="width:260px;">
                                <table summary="combobox" style="border-width:0;border-collapse:collapse;" class="">
                                    <tbody>
                                        <tr>
                                            <td style="width:100%;">
                                               <asp:DropDownList CssClass="ddl" ID="ddlEdad" runat="server" Width="250px"  AutoPostBack="true" ></asp:DropDownList>
                                            </td> 
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
              </div>
      </div>


           <div style="width: 440px; height: 25px; float: left; line-height: 25px; margin-bottom: 10px;">
          <div style="width: 140px; height: 25px; float: left; line-height: 25px; text-align: right;">
                            <label style="color: #003399;">*</label>
                            <label>Sexo:</label>
           </div>

          <div style="width: 260px; height: 25px; float: left; margin-left: 25px; line-height: 25px;">
                <div style="width:260px;">
                        <table summary="combobox" style="border-width:0;border-collapse:collapse;" class="">
                            <tbody>
                                <tr>
                                    <td style="width:100%;" >
                                        <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal"> 
                                            <asp:listitem Value="0" Selected="True">Hombre</asp:listitem> 
                                            <asp:listitem Value="1" Selected="False">Mujer</asp:listitem>

                                        </asp:RadioButtonList>
                                                
                                    </td> 
                                </tr>
                            </tbody>
                        </table>                
                    </div>
              </div>
          </div>      
           <br />
        </div>


        <br />
        <br />

            <div id="barraContacto" style="width:auto; height: auto; margin-bottom: 0; text-align:center; font-family:Calibri" >
                <img alt="" class="auto-style1" src="Images/PLECA-AZUL.png" style="width:828px; height:8px" />
            </div>

        <br />
            <div id="formulairoContacto" style="width: 450px; height: auto; margin-bottom: 0; text-align:center; margin-left:450px; font-family:Calibri" >
            <div style="width: 440px; height: 25px; float: left; line-height: 25px; margin-bottom: 10px;">
            <div style="width: 140px; height: 25px; float: left; line-height: 25px; text-align: right;">
                            <label style="color: #003399;">*</label>
                            <label>Teléfono:</label>
           </div>
           <div style="width: 260px; height: 25px; float: left; margin-left: 25px; line-height: 25px;">
           <div style="width:260px;">
                                <table summary="combobox" style="border-width:0;border-collapse:collapse;" class="">
                                    <tbody>
                                        <tr>
                                            <td style="width:100%;">
                                                <asp:TextBox runat="server" Width="250px" ID="txtTelefono" CssClass="ddl" MaxLength="10" OnTextChanged="txtTelefono_TextChanged"></asp:TextBox>
                                               
                                            </td> 
                                        </tr>
                                    </tbody>
                                </table>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtTelefono" ValidationExpression="^[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$"></asp:RegularExpressionValidator>
                            </div>
               <script>
                   //Función que permite solo Números
                   function ValidaSoloNumeros() {
                       if ((event.keyCode < 48) || (event.keyCode > 57))
                           event.returnValue = false;
                   }
               </script>
              </div>
      </div>
      
              <br />

                <div style="width: 440px; height: 25px; float: left; line-height: 25px; margin-bottom: 10px;">
          <div style="width: 140px; height: 25px; float: left; line-height: 25px; text-align: right;">
                            <label style="color: #003399;">*</label>
                            <label>E-mail</label>
           </div>

          <div style="width: 260px; height: 25px; float: left; margin-left: 25px; line-height: 25px;">
           <div style="width:260px;">
                                <table summary="combobox" style="border-width:0;border-collapse:collapse;" class="">
                                    <tbody>
                                        <tr>
                                            <td style="width:100%;">
                                                <asp:TextBox runat="server" Width="250px" ID="txtCorreo" CssClass="ddl" ></asp:TextBox>
                                               
                                            </td> 
                                        </tr>
                                    </tbody>
                                </table>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtCorreo" ValidationExpression="^([a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]){1,70}$" ErrorMessage="Correo no válido"></asp:RegularExpressionValidator>
                            </div>
              </div>
      </div>

          </div>

<br />
        <br />
        <br />
       <div id="divbtnCotiza" style="width: 450px; height: auto; margin-bottom: 0; margin-top:30px; text-align:center; margin-left:450px; font-family:Calibri" >
            <asp:ImageButton ID="ImgCotiza" runat="server" ImageUrl="~/Images/blue-button.png" Width="200px" Height="40px" AlternateText="Cotiza" OnClick="ImgCotiza_Click" />
           </div>
           
    </div>
        <div style="height:200px; width:100%" >
        </div>

</asp:Content>
