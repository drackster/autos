﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master1.Master" AutoEventWireup="true" CodeBehind="CotizacionAuto.aspx.cs" Inherits="CotizadorAutos.CotizacionAuto" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="font-family:Calibri">
    

        <div style="text-align:center; margin-left:50px">
            <img src="Images/PLECA-AZUL.png" />
        </div>
        <br />
        <div style="text-align:center; margin-left:50px">
          <div>              
              <div style="width:300PX; float:right; text-align:right; margin-right:200px">
                <asp:Label ID="lblCotizacion" runat="server" Text="No. de cotización:"></asp:Label>
                  <asp:Label ID="lblCotizacion1" runat="server"></asp:Label>
              </div>
          </div>  
            
            <br />
        <div style="text-align:center; margin-left:650px;">
            <div style="text-align:left; font:900">
              <asp:Label ID="Label1" runat="server" Text="Datos del vehículo cotizado"></asp:Label>
                </div>
          <div>
            <table style="text-align:right">
                <tbody>
                    <tr>
                        <td style="width:100px">
                            Marca:
                        </td>
                        <td>
                           
                            <asp:Label ID="lblMarca" runat="server"></asp:Label>
                        </td>
                    
                    </tr>
                </tbody>
            </table>  
           </div>     
               <div>
            <table style="text-align:right">
                <tbody>
                    <tr>
                        <td style="width:100px">
                            Submarca:
                        </td>
                        <td>
                             <asp:Label ID="lblsubmarca" runat="server"></asp:Label>
                        </td>
                    </tr>
                </tbody>
            </table>  
           </div>     
               <div>
            <table style="text-align:right">
                <tbody>
                    <tr>
                        <td style="width:100px">
                            Año:
                        </td>
                        <td>
                            <asp:Label ID="lblaño" runat="server" ></asp:Label>
                        </td>
                    </tr>
                </tbody>
            </table>  
           </div>     
               <div>
            <table style="text-align:right">
                <tbody>
                    <tr>
                        <td style="width:100px">
                            Versión:
                        </td>
                        <td>
                            <asp:Label ID="lblVersion" runat="server"></asp:Label>
                        </td>
                    </tr>
                </tbody>
            </table>  
           </div>             
        </div>

           <div style="text-align:center; margin-left:50px">
            <img src="Images/PLECA-AZUL.png" />
          </div>
        <div style="text-align:center; margin-left:100px">
            <asp:Label ID="Label2" runat="server" Text="Condiciones del seguro"></asp:Label>

        </div>
            <br />
            <div id="divPrimas" style="margin-left:160px" >
                <table id="imgAseg">
                    <tr>
                           <td >

                        </td>

                        <td style="width:143px">

                        </td>
                        <td style="width:143px;text-align:center">
                            <asp:Image ID="ImgAseg1" runat="server" Width="100" Height="50" />
                        </td>

                        <td style="width:143px;text-align:center">
                            <asp:Image ID="ImgAseg2" runat="server" Width="100" Height="50"/>
                        </td>

                        <td style="width:143px;text-align:center">
                            <asp:Image ID="ImgAseg3" runat="server" Width="100" Height="50" />
                        </td>

                        <td style="width:143px;text-align:center">
                            <asp:Image ID="ImgAseg4" runat="server" Width="100" Height="50"/>
                        </td>

                        <td style="width:143px;text-align:center">
                            <asp:Image ID="ImgAseg5" runat="server" Width="100" Height="50"/>
                        </td>

                        <td style="width:143px;text-align:center">
                            <asp:Image ID="ImgAseg6" runat="server" Width="100" Height="50"/>
                        </td>
                    </tr>
                    </table>


                <table id="contado">
                    <tr style="height:40px; background-color:yellowgreen">
                           <td >

                        </td>
                        <td style="width:143px; text-align:right; background-color:yellowgreen">
                             <asp:Label ID="Label3" runat="server" Text="Contado "></asp:Label>
                        </td>

                        <td style="width:143px;text-align:center">                           
                            <asp:Label ID="lbContadoAseg1" runat="server"></asp:Label>
                        </td> 
                        
                       <td style="width:143px;text-align:center">                           
                            <asp:Label ID="lbContadoAseg2" runat="server"></asp:Label>
                        </td> 
                        
                         <td style="width:143px;text-align:center">                           
                             <asp:Label ID="lbContadoAseg3" runat="server"></asp:Label>
                         </td> 
                        
                        <td style="width:143px;text-align:center">                           
                            <asp:Label ID="lbContadoAseg4" runat="server"></asp:Label>
                        </td>

                         <td style="width:143px;text-align:center">                           
                            <asp:Label ID="lbContadoAseg5" runat="server"></asp:Label>
                        </td>

                         <td style="width:143px;text-align:center">                           
                            <asp:Label ID="lbContadoAseg6" runat="server"></asp:Label>
                        </td>

                    </tr>
              </table>

                <table id="Semestral">
                    <tr style="height:40px; background-color:yellowgreen; border:1px hidden">
                        
                        <td style="text-align:inherit">
                       
                        </td>


                        <td style="width:143px;text-align:right; background-color:yellowgreen">
                           
                             <asp:Label ID="lbAmpliaSEP" runat="server" Text="Semestral "></asp:Label>
                        </td>

                        <td style="width:143px;text-align:center">                           
                            <asp:Label ID="lbSemestralAseg1" runat="server"></asp:Label>
                        </td> 
                        
                        <td style="width:143px;text-align:center">                           
                            <asp:Label ID="lbSemestralAseg2" runat="server"></asp:Label>
                        </td>
                        
                         <td style="width:143px;text-align:center">                           
                             <asp:Label ID="lbSemestralAseg3" runat="server"></asp:Label>
                         </td> 
                        
                        <td style="width:143px;text-align:center">                           
                            <asp:Label ID="lbSemestralAseg4" runat="server"></asp:Label>
                        </td>

                        <td style="width:143px;text-align:center">                           
                            <asp:Label ID="lbSemestralAseg5" runat="server"></asp:Label>
                        </td>

                        <td style="width:143px;text-align:center">                           
                            <asp:Label ID="lbSemestralAseg6" runat="server"></asp:Label>
                        </td>
                    </tr>

                   
          
                    <tr id="1pSem">
                           <td>

                        </td>
                   
                        <td style="width:143px;text-align:right">
                             <asp:Label ID="lbAmpliaSEP0" runat="server" Text="Primer"></asp:Label>
                        </td>

                        <td style="width:143px;text-align:center">                           
                            <asp:Label ID="lbSemestralAseg1P1" runat="server"></asp:Label>
                        </td> 
                        
                        <td style="width:143px;text-align:center">                           
                            <asp:Label ID="lbSemestralAseg2P1" runat="server"></asp:Label>
                        </td>
                        
                         <td style="width:143px;text-align:center">                           
                             <asp:Label ID="lbSemestralAseg3P1" runat="server"></asp:Label>
                         </td> 
                        
                        <td style="width:143px;text-align:center">                           
                            <asp:Label ID="lbSemestralAseg4P1" runat="server"></asp:Label>
                        </td>

                        <td style="width:143px;text-align:center">                           
                            <asp:Label ID="lbSemestralAseg5P1" runat="server"></asp:Label>
                        </td>

                        <td style="width:143px;text-align:center">                           
                            <asp:Label ID="lbSemestralAseg6P1" runat="server"></asp:Label>
                        </td>


                    </tr>
          
                    <tr id="SubSem" style="text-align:center" >
                        <td>

                        </td>
                       
                        <td style="width:143px;text-align:right">
                             <asp:Label ID="lbAmpliaSEP1" runat="server" Text="Subsecuentes"></asp:Label>
                        </td>

                        <td style="width:143px;text-align:center">                           
                            <asp:Label ID="lbSemestralAseg1Sub" runat="server"></asp:Label>
                        </td> 
                        
                        <td style="width:143px;text-align:center">                           
                            <asp:Label ID="lbSemestralAseg2Sub" runat="server"></asp:Label>
                        </td>
                        
                         <td style="width:143px;text-align:center">                           
                             <asp:Label ID="lbSemestralAseg3Sub" runat="server"></asp:Label>
                         </td> 
                        
                        <td style="width:143px;text-align:center">  
                            <asp:Label ID="lbSemestralAseg4Sub" runat="server"></asp:Label>
                        </td>

                        <td style="width:143px;text-align:center">
                            <asp:Label ID="lbSemestralAseg5Sub" runat="server"></asp:Label>
                        </td>

                        <td style="width:143px;text-align:center">
                            <asp:Label ID="lbSemestralAseg6Sub" runat="server"></asp:Label>
                        </td>


                    </tr>
                </table>
                <br />
            </div>
           <br />

            <div>
                 <asp:Button ID="btnAmpliaEP" runat="server" Text="Amplia EP"  Width="100px" OnClick="btnAmpliaEP_Click"/>
                <asp:Button ID="btnAmplia" runat="server" Text="Amplia"  Width="100px" OnClick="btnAmplia_Click" />
                <asp:Button ID="btnLimitadaEP" runat="server" Text="Limitada EP"  Width="100px" OnClick="btnLimitadaEP_Click" />
                <asp:Button ID="btnLimitada" runat="server" Text="Limitada"  Width="100px" OnClick="btnLimitada_Click" />
            </div>
         <br />
          <table style="margin:auto">
              <tr>
                  <td >
                       <asp:GridView ID="gwCoberturas" runat="server" CssClass="ui-table"  Width="1000px">  </asp:GridView>
                 </td>
               </tr>
           </table>
        
            <div >

               
                
            </div>
       

        </div>


    </div>
</asp:Content>
