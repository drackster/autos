﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master1.Master" AutoEventWireup="True" CodeBehind="DatosCliente.aspx.cs" Inherits="CotizadorAutos.DatosCliente" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="style/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="style/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
    <link href="style/bootstrap/css/bootstrap-responsive.css" rel="stylesheet" />
    <div Class="active embed-responsive" style="text-align:center">
        
       <div>
            <asp:RadioButton ID="rbAutos" runat="server" Checked="true" Visible="false" />
            <asp:RadioButton ID="rbCamiones" runat="server" Checked="false" Visible="false" />
        </div>
        <br />
        <br />
        <div id="barraAuto" style="width: auto; height: auto; margin-bottom: 0; text-align: center; font-family: Calibri">
            <img alt="" class="img-responsive" src="Images/PLECA-AZUL.png" style="width: 828px; height: 8px" />
        </div>
        <br />

        <div id="formulairoAuto" style="width: 450px; height: auto; margin-bottom: 0; text-align: center; margin-left: 450px; font-family: Calibri">
            <div style="width: 440px; height: 25px; float: left; line-height: 25px; margin-bottom: 10px;">
                <div style="width: 140px; height: 25px; float: left; line-height: 25px; text-align: right;">
                  <%--  <label style="color: #003399;">*</label>--%>
                    <label>Marca:</label>
                </div>
                <div style="width: 260px; height: 25px; float: left; margin-left: 25px; line-height: 25px;">
                    <div style="width: 260px;">
                        <table summary="combobox" style="border-width: 0; border-collapse: collapse;" class="">
                            <tbody>
                                <tr>
                                    <td style="width: 100%;">
                                        <asp:ScriptManager ID="ScriptManager2" runat="server"></asp:ScriptManager>
                                        <asp:updatepanel id="UpdatePanel2" runat="server">
                                        <contenttemplate>
                                          <asp:DropDownList CssClass="dropdown-toggle" ID="ddlMarca" runat="server" Width="250px" OnSelectedIndexChanged="ddlMarca_SelectedIndexChanged" AutoPostBack="true" ViewStateMode="Enabled"></asp:DropDownList>
                                        </contenttemplate>
                                        </asp:updatepanel>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div style="width: 440px; height: 25px; float: left; line-height: 25px; margin-bottom: 10px;">
                <div style="width: 140px; height: 25px; float: left; line-height: 25px; text-align: right;">
                   <%-- <label style="color: #003399;">*</label>--%>
                    <label>SubMarca:</label>
                </div>
                <div style="width: 260px; height: 25px; float: left; margin-left: 25px; line-height: 25px;">
                    <div style="width: 260px;">
                        <table summary="combobox" style="border-width: 0; border-collapse: collapse;" class="">
                            <tbody>
                                <tr>
                                    <td style="width: 100%;">                             
                                        <asp:updatepanel id="UpdatePanel1" runat="server">
                                        <contenttemplate>
                                           <asp:DropDownList CssClass="dropdown-toggle" ID="ddlSubmarca" runat="server" Width="250px" OnSelectedIndexChanged="ddlSubmarca_SelectedIndexChanged" AutoPostBack="true" ViewStateMode="Enabled"></asp:DropDownList>
                                        </contenttemplate>
                                        </asp:updatepanel>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div style="width: 440px; height: 25px; float: left; line-height: 25px; margin-bottom: 10px;">
                <div style="width: 140px; height: 25px; float: left; line-height: 25px; text-align: right;">
                    <%--<label style="color: #003399;">*</label>--%>
                    <label>Año:</label>
                </div>
                <div style="width: 260px; height: 25px; float: left; margin-left: 25px; line-height: 25px;">
                    <div id="ctl00_ContentPlaceHolder1_rdcMarca" style="width: 260px;">
                        <table summary="combobox" style="border-width: 0; border-collapse: collapse;" class="">
                            <tbody>
                                <tr>
                                    <td style="width: 100%;">
                                        <asp:updatepanel id="UpdatePanel3" runat="server">
                                        <contenttemplate>
                                           <asp:DropDownList CssClass="dropdown-toggle" ID="ddlaño" runat="server" Width="250px" OnSelectedIndexChanged="ddlaño_SelectedIndexChanged" AutoPostBack="true" ViewStateMode="Enabled"></asp:DropDownList>
                                        </contenttemplate>
                                        </asp:updatepanel>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div style="width: 440px; height: 25px; float: left; line-height: 25px; margin-bottom: 10px;">
                <div style="width: 140px; height: 25px; float: left; line-height: 25px; text-align: right;">
                <%--    <label style="color: #003399;">*</label>--%>
                    <label>Descripción:</label>
                </div>
                <div style="width: 260px; height: 25px; float: left; margin-left: 25px; line-height: 25px;">
                    <div style="width: 260px;">
                        <table summary="combobox" style="border-width: 0; border-collapse: collapse;" class="">
                            <tbody>
                                <tr>
                                    <td style="width: 100%;">                                        
                                        <asp:updatepanel id="UpdatePanel4" runat="server">
                                        <contenttemplate>
                                              <asp:DropDownList CssClass="dropdown-toggle" ID="ddldescripcion" runat="server" Width="250px" OnSelectedIndexChanged="ddldescripcion_SelectedIndexChanged" AutoPostBack="true" ViewStateMode="Enabled"></asp:DropDownList>
                                        </contenttemplate>
                                        </asp:updatepanel>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


        </div>
        <br />
        <div id="barraUbicacion" style="width: auto; height: auto; margin-bottom: 0; text-align: center; font-family: Calibri">


            <img alt="" class="img-responsive"  src="Images/PLECA-AZUL.png" style="width: 828px; height: 8px" />
        </div>
        <br />
        <div id="formulairoUbicacion" style="width: 450px; height: auto; margin-bottom: 0; text-align: center; margin-left: 450px; font-family: Calibri">

            <div style="width: 440px; height: 25px; float: left; line-height: 25px; margin-bottom: 10px;">
                <div style="width: 140px; height: 25px; float: left; line-height: 25px; text-align: right;">
                   <%-- <label style="color: #003399;">*</label>--%>
                    <label>Código Postal:</label>
                </div>


                <div style="width: 260px; height: 25px; float: left; margin-left: 25px; line-height: 25px;">
                    <div style="width: 260px;">
                        <table summary="combobox" style="border-width: 0; border-collapse: collapse;" class="">
                            <tbody>
                                <tr>
                                    <td style="width: 100%;">
                                        <asp:updatepanel id="UpdatePanel5" runat="server">
                                        <contenttemplate>
                                             <%--<input type="text" class="form-control input-lg" id="txtCP" style="width:250px" ro runat="server">--%>
                                         <asp:TextBox ID="txtCP" runat="server" Width="250px" Height="30px" OnTextChanged="txtCP_TextChanged" AutoPostBack="true" CssClass="ddl" MaxLength="5"></asp:TextBox>
                                        </contenttemplate>
                                        </asp:updatepanel>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div id="divEstado" runat="server" style="width: 440px; height: 25px; float: left; line-height: 25px; margin-bottom: 10px;" >
                <div style="width: 140px; height: 25px; float: left; line-height: 25px; text-align: right;">
               <%--     <label style="color: #003399;">*</label>--%>
                    <label>Estado:</label>
                </div>

                <div style="width: 260px; height: 25px; float: left; margin-left: 25px; line-height: 25px;">
                    <div style="width: 260px;">
                        <table summary="combobox" style="border-width: 0; border-collapse: collapse;" class="">
                            <tbody>
                                <tr>
                                    <td style="width: 100%;">
                                        <asp:updatepanel id="UpdatePanel6" runat="server">
<contenttemplate>
                                        <asp:DropDownList CssClass="dropdown-toggle" ID="ddlEstado" runat="server" Width="250px" OnSelectedIndexChanged="ddlEstado_SelectedIndexChanged" AutoPostBack="true" ViewStateMode="Enabled"></asp:DropDownList>
    </contenttemplate>
</asp:updatepanel>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div id="divMunicipio" runat="server" style="width: 440px; height: 25px; float: left; line-height: 25px; margin-bottom: 10px;" >
                <div style="width: 140px; height: 25px; float: left; line-height: 25px; text-align: right;">
                 <%--   <label style="color: #003399;">*</label>--%>
                    <label>Municipio:</label>
                </div>

                <div style="width: 260px; height: 25px; float: left; margin-left: 25px; line-height: 25px;">
                    <div style="width: 260px;">
                        <table summary="combobox" style="border-width: 0; border-collapse: collapse;" class="">
                            <tbody>
                                <tr>
                                    <td style="width: 100%;">
                                        <asp:updatepanel id="UpdatePanel7" runat="server">
<contenttemplate>
                                        <asp:DropDownList CssClass="dropdown-toggle" ID="ddlMunicipio" runat="server" Width="250px" AutoPostBack="true" ViewStateMode="Enabled" OnSelectedIndexChanged="ddlMunicipio_SelectedIndexChanged"></asp:DropDownList>
    </contenttemplate>
</asp:updatepanel>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div id="divColonia" runat="server" style="width: 440px; height: 25px; float: left; line-height: 25px; margin-bottom: 10px;" >
                <div style="width: 140px; height: 25px; float: left; line-height: 25px; text-align: right;">
                   <%-- <label style="color: #003399;">*</label>--%>
                    <label>Colonia:</label>
                </div>

                <div style="width: 260px; height: 25px; float: left; margin-left: 25px; line-height: 25px;">
                    <div style="width: 260px;">
                        <table summary="combobox" style="border-width: 0; border-collapse: collapse;" class="">
                            <tbody>
                                <tr>
                                    <td style="width: 100%;">
                                        <asp:updatepanel id="UpdatePanel8" runat="server">
<contenttemplate>
                                        <asp:DropDownList CssClass="dropdown-toggle" ID="ddlColonias" runat="server" Width="250px" OnSelectedIndexChanged="ddlColonias_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
    </contenttemplate>
</asp:updatepanel>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
        <br />

        <div id="barraCliente" style="width: auto; height: auto; margin-bottom: 0; text-align: center; font-family: Calibri">


            <img alt="" class="img-responsive"  src="Images/PLECA-AZUL.png" style="width: 828px; height: 8px" />
        </div>
        <br />

        <div id="formulairoCliente" style="width: 450px; height: auto; margin-bottom: 0; text-align: center; margin-left: 450px; font-family: Calibri">

            <div style="width: 440px; height: 25px; float: left; line-height: 25px; margin-bottom: 10px;">
                <div style="width: 140px; height: 25px; float: left; line-height: 25px; text-align: right;">
   <%--                 <label style="color: #003399;">*</label>--%>
                    <label>Nombre:</label>
                </div>



                <div style="width: 260px; height: 25px; float: left; margin-left: 25px; line-height: 25px;">
                    <div style="width: 260px;">
                        <table summary="combobox" style="border-width: 0; border-collapse: collapse;" class="">
                            <tbody>
                                <tr>
                                    <td style="width: 100%;">
                                        
                                        <asp:TextBox runat="server" Width="250px" Height="30px" ID="txtNombre" CssClass="ddl"></asp:TextBox>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div style="width: 440px; height: 25px; float: left; line-height: 25px; margin-bottom: 10px;">
                <div style="width: 140px; height: 25px; float: left; line-height: 25px; text-align: right;">
                    <%--<label style="color: #003399;">*</label>--%>
                    <label>Apellido:</label>
                </div>

                <div style="width: 260px; height: 25px; float: left; margin-left: 25px; line-height: 25px;">
                    <div style="width: 260px;">
                        <table summary="combobox" style="border-width: 0; border-collapse: collapse;" class="">
                            <tbody>
                                <tr>
                                    <td style="width: 100%;">
                                        <asp:TextBox runat="server" Width="250px" Height="30px" ID="txtApellido" CssClass="ddl"></asp:TextBox>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div style="width: 440px; height: 25px; float: left; line-height: 25px; margin-bottom: 10px;">
                <div style="width: 140px; height: 25px; float: left; line-height: 25px; text-align: right;">
                    <%--<label style="color: #003399;">*</label>--%>
                    <label>Edad:</label>
                </div>

                <div style="width: 260px; height: 25px; float: left; margin-left: 25px; line-height: 25px;">
                    <div style="width: 260px;">
                        <table summary="combobox" style="border-width: 0; border-collapse: collapse;" class="">
                            <tbody>
                                <tr>
                                    <td style="width: 100%;">
                                    <asp:updatepanel id="UpdatePanel9" runat="server">
                                    <contenttemplate>
                                        <asp:DropDownList CssClass="dropdown-toggle" ID="ddlEdad" runat="server" Width="250px" AutoPostBack="true"></asp:DropDownList>
                                    </contenttemplate>
                                    </asp:updatepanel>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


            <div style="width: 440px; height: 25px; float: left; line-height: 25px; margin-bottom: 10px;">
                <div style="width: 140px; height: 25px; float: left; line-height: 25px; text-align: right;">
                    <%--<label style="color: #003399;">*</label>--%>
                    <label>Sexo:</label>
                </div>

                <div style="width: 260px; height: 25px; float: left; margin-left: 25px; line-height: 25px;">
                    <div style="width: 260px;">
                        <table summary="combobox" style="border-width: 0; border-collapse: collapse;" class="">
                            <tbody>
                                <tr>
                                    <td style="width: 100%;">
                                        <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Value="0" Selected="True">Hombre</asp:ListItem>
                                            <asp:ListItem Value="1" Selected="False">Mujer</asp:ListItem>

                                        </asp:RadioButtonList>

                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <br />
        </div>


        <br />
        <br />

        <div id="barraContacto" style="width: auto; height: auto; margin-bottom: 0; text-align: center; font-family: Calibri">
            <img alt="" class="img-responsive"  src="Images/PLECA-AZUL.png" style="width: 828px; height: 8px" />
        </div>

        <br />
        <div id="formulairoContacto" style="width: 450px; height: auto; margin-bottom: 0; text-align: center; margin-left: 450px; font-family: Calibri">
            <div style="width: 440px; height: 25px; float: left; line-height: 25px; margin-bottom: 10px;">
                <div style="width: 140px; height: 25px; float: left; line-height: 25px; text-align: right;">
                   <%-- <label style="color: #003399;">*</label>--%>
                    <label>Teléfono:</label>
                </div>
                <div style="width: 260px; height: 25px; float: left; margin-left: 25px; line-height: 25px;">
                    <div style="width: 260px;">
                        <table summary="combobox" style="border-width: 0; border-collapse: collapse;" class="">
                            <tbody>
                                <tr>
                                    <td style="width: 100%;">
                                        <asp:TextBox runat="server" Width="250px" Height="30px" ID="txtTelefono" CssClass="ddl" MaxLength="10" OnTextChanged="txtTelefono_TextChanged"></asp:TextBox>
                                        <%-- <input type="tel"  class="ddl" id="txtTelefono" runat="server"  />--%>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtTelefono" ValidationExpression="^[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$"></asp:RegularExpressionValidator>
                    </div>
                    <script>
                        //Función que permite solo Números
                        function ValidaSoloNumeros() {
                            if ((event.keyCode < 48) || (event.keyCode > 57))
                                event.returnValue = false;
                        }
                    </script>
                </div>
            </div>

            <br />

            <div style="width: 440px; height: 25px; float: left; line-height: 25px; margin-bottom: 10px;">
                <div style="width: 140px; height: 25px; float: left; line-height: 25px; text-align: right;">
                   <%-- <label style="color: #003399;">*</label>--%>
                    <label>E-mail</label>
                </div>

                <div style="width: 260px; height: 25px; float: left; margin-left: 25px; line-height: 25px;">
                    <div style="width: 260px;">
                        <table summary="combobox" style="border-width: 0; border-collapse: collapse;" class="">
                            <tbody>
                                <tr>
                                    <td style="width: 100%;">
                                        <asp:TextBox runat="server" Width="250px" Height="30px" ID="txtCorreo" CssClass="ddl"></asp:TextBox>

                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtCorreo" ValidationExpression="^([a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]){1,70}$" ErrorMessage="Correo no válido"></asp:RegularExpressionValidator>
                    </div>
                </div>
            </div>
            <div id="divProgres" style="position: absolute; top: 500px; left: 650px; display: none; box-shadow: 10px 10px 5px #888888;">

                <br />
                <img src="Images/loader.gif" alt="cargando" />
            </div>
            <script>
                function progres() {

                    document.getElementById('divProgres').style.display = "block";
                }
            </script>
        </div>

        <br />
        <div >
                        <asp:updatepanel id="UpdatePanel10" runat="server">
<contenttemplate>
            <asp:CheckBox ID="CheckBox1" runat="server" Text="He leido y estoy de acuerdo con el aviso de privacidad"  OnCheckedChanged="CheckBox1_CheckedChanged" AutoPostBack="true" />
        </contenttemplate>
</asp:updatepanel>
        </div>
      
        <div id="divbtnCotiza" class="container" style="width:100%; text-align: center; font-family: Calibri;">
                        <asp:updatepanel id="UpdatePanel11" runat="server">
<contenttemplate>
               <asp:Button ID="ImgCotiza" Style="margin-left:-100px" runat="server" Text="Cotizar" CssClass="btn btn-primary"  Width="250px" OnClick="ImgCotiza_Click" />
        </contenttemplate>
</asp:updatepanel>
          <%--  <asp:ImageButton ID="ImgCotiza" runat="server" ImageUrl="~/Images/blue-button.png" Width="200px" Height="40px" AlternateText="Cotiza" OnClick="ImgCotiza_Click" OnClientClick="progres()" />--%>
        </div>

        <br />


    </div>
    <script src="bootstrap/js/bootstrap.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
</asp:Content>

