﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master1.Master" AutoEventWireup="true" CodeBehind="CotizacionAuto.aspx.cs" Inherits="CotizadorAutos.CotizacionAuto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <link href="style/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <div style="font-family: Calibri">


        <div style="text-align: center; margin-left: 50px">
            <img src="Images/PLECA-AZUL.png" />
        </div>
        <br />
        <div style="text-align: center; margin-left: 50px">
            <div>

                <div style="width: 300PX; float: right; text-align: right; margin-right: 200px">
                    <asp:Label ID="lblCotizacion" runat="server" Text="No. de cotización:"></asp:Label>
                    <asp:Label ID="lblCotizacion1" runat="server"></asp:Label>
                </div>

            </div>

            <br />
            <div style="text-align: center; margin-left: 650px;">
                <div style="text-align: left; font: 900">
                    <asp:Label ID="Label1" runat="server" Text="Datos del vehículo cotizado" Font-Bold="true"></asp:Label>
                </div>
                <div>
                    <table style="text-align: right">
                        <tbody>
                            <tr>
                                <td style="width: 100px">Marca:
                                </td>
                                <td>

                                    <asp:Label ID="lblMarca" runat="server"></asp:Label>
                                </td>

                            </tr>
                        </tbody>
                    </table>
                </div>
                <div>
                    <table style="text-align: right">
                        <tbody>
                            <tr>
                                <td style="width: 100px">Submarca:
                                </td>
                                <td>
                                    <asp:Label ID="lblsubmarca" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div>
                    <table style="text-align: right">
                        <tbody>
                            <tr>
                                <td style="width: 100px">Año:
                                </td>
                                <td>
                                    <asp:Label ID="lblaño" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div>
                    <table style="text-align: right">
                        <tbody>
                            <tr>
                                <td style="width: 100px">Versión:
                                </td>
                                <td>
                                    <asp:Label ID="lblVersion" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div style="text-align: center; margin-left: 50px">
                <img src="Images/PLECA-AZUL.png" />
            </div>
            <div style="text-align: center; margin-left: 100px">
                <asp:Label ID="Label2" runat="server" Text="Condiciones del seguro"></asp:Label>

            </div>
            <div id="divPrimas" class="container" runat="server" style="margin-left: 250px" visible="false">
                <div>
                    <table id="NoCotizacion">
                        <tr>
                            <td></td>

                            <td style="width: 143px"></td>
                            <td style="width: 143px; text-align: center">
                                <asp:Label ID="CotAna" runat="server" Text="numcot"></asp:Label>
                            </td>

                            <td style="width: 143px; text-align: center">
                                <asp:Label ID="Label5" runat="server" Text="numcot"></asp:Label>
                            </td>

                            <td style="width: 143px; text-align: center">
                                <asp:Label ID="Label9" runat="server" Text="numcot"></asp:Label>
                            </td>

                            <td style="width: 143px; text-align: center">
                                <asp:Label ID="Label6" runat="server" Text="numcot"></asp:Label>
                            </td>

                            <td style="width: 143px; text-align: center">
                                <asp:Label ID="Label7" runat="server" Text="numcot"></asp:Label>
                            </td>

                            <td style="width: 143px; text-align: center">
                                <asp:Label ID="Label8" runat="server" Text="numcot"></asp:Label>
                            </td>
                        </tr>
                    </table>
                     <table id="pdf">
                        <tr>
                            <td></td>

                            <td style="width: 143px"></td>
                            <td style="width: 143px; text-align: center">
                                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="Images/pdf.jpg" Width="38px" Height="46px" OnClick="ImageButton1_Click" />
                            </td>

                            <td style="width: 143px; text-align: center">
                                <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="Images/pdf.jpg" Width="38px" Height="46px" />
                            </td>

                            <td style="width: 143px; text-align: center">
                                <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="Images/pdf.jpg" Width="38px" Height="46px" />
                            </td>

                            <td style="width: 143px; text-align: center">
                                 <asp:ImageButton ID="ImageButton4" runat="server" ImageUrl="Images/pdf.jpg" Width="38px" Height="46px" />
                            </td>

                            <td style="width: 143px; text-align: center">
                                <asp:ImageButton ID="ImageButton5" runat="server" ImageUrl="Images/pdf.jpg" Width="38px" Height="46px" />
                            </td>

                            <td style="width: 143px; text-align: center">
                                <asp:ImageButton ID="ImageButton6" runat="server" ImageUrl="Images/pdf.jpg" Width="38px" Height="46px" />
                            </td>
                        </tr>
                    </table>
                </div>
                <br />

                <table id="imgAseg">
                    <tr>
                        <td></td>

                        <td style="width: 143px"></td>
                     
                        <td style="width: 143px; text-align: center">
                            <asp:Image ID="ImgAseg1" data-toggle="tooltip" data-placement="right" title="Hooray!" runat="server" Width="100" Height="50" />
                        </td>

                        <td style="width: 143px; text-align: center">
                            <asp:Image ID="ImgAseg2" runat="server" Width="100" Height="50" />
                        </td>

                        <td style="width: 143px; text-align: center">
                            <asp:Image ID="ImgAseg3" runat="server" Width="100" Height="50" />
                        </td>

                        <td style="width: 143px; text-align: center">
                            <asp:Image ID="ImgAseg4" runat="server" Width="100" Height="50" />
                        </td>

                        <td style="width: 143px; text-align: center">
                            <asp:Image ID="ImgAseg5" runat="server" Width="100" Height="50" />
                        </td>

                        <td style="width: 143px; text-align: center">
                            <asp:Image ID="ImgAseg6" runat="server" Width="100" Height="50" />
                        </td>
                    </tr>
                </table>


                <table id="contado">
                    <tr style="height: 40px; background-color: #A9BCF5">
                        <td></td>
                        <td style="width: 143px; text-align: right; background-color: #A9BCF5">
                            <asp:Label ID="Label3" runat="server" Text="Contado "></asp:Label>
                        </td>

                        <td style="width: 143px; text-align: center">
                            <asp:Label ID="lbContadoAseg1" runat="server"></asp:Label>
                        </td>

                        <td style="width: 143px; text-align: center">
                            <asp:Label ID="lbContadoAseg2" runat="server"></asp:Label>
                        </td>

                        <td style="width: 143px; text-align: center">
                            <asp:Label ID="lbContadoAseg3" runat="server"></asp:Label>
                        </td>

                        <td style="width: 143px; text-align: center">
                            <asp:Label ID="lbContadoAseg4" runat="server"></asp:Label>
                        </td>

                        <td style="width: 143px; text-align: center">
                            <asp:Label ID="lbContadoAseg5" runat="server"></asp:Label>
                        </td>

                        <td style="width: 143px; text-align: center">
                            <asp:Label ID="lbContadoAseg6" runat="server"></asp:Label>
                        </td>

                    </tr>
                </table>
                <br />
                <table id="Semestral" style="text-align: center">
                    <tr style="height: 40px; background-color: #A9BCF5; border: 1px hidden">

                        <td style="text-align: inherit"></td>


                        <td style="width: 143px; text-align: right; background-color: #A9BCF5">

                            <asp:Label ID="lbAmpliaSEP" runat="server" Text="Semestral "></asp:Label>
                        </td>

                        <td style="width: 143px; text-align: center">
                            <asp:Label ID="lbSemestralAseg1" runat="server"></asp:Label>
                        </td>

                        <td style="width: 143px; text-align: center">
                            <asp:Label ID="lbSemestralAseg2" runat="server"></asp:Label>
                        </td>

                        <td style="width: 143px; text-align: center">
                            <asp:Label ID="lbSemestralAseg3" runat="server"></asp:Label>
                        </td>

                        <td style="width: 143px; text-align: center">
                            <asp:Label ID="lbSemestralAseg4" runat="server"></asp:Label>
                        </td>

                        <td style="width: 143px; text-align: center">
                            <asp:Label ID="lbSemestralAseg5" runat="server"></asp:Label>
                        </td>

                        <td style="width: 143px; text-align: center">
                            <asp:Label ID="lbSemestralAseg6" runat="server"></asp:Label>
                        </td>
                    </tr>



                    <tr id="1pSem">
                        <td></td>

                        <td style="width: 143px; text-align: right; text-size-adjust: auto">
                            <asp:Label ID="lbAmpliaSEP0" runat="server" Text="Primer pago"></asp:Label>
                        </td>

                        <td style="width: 143px; text-align: center">
                            <asp:Label ID="lbSemestralAseg1P1" runat="server"></asp:Label>
                        </td>

                        <td style="width: 143px; text-align: center">
                            <asp:Label ID="lbSemestralAseg2P1" runat="server"></asp:Label>
                        </td>

                        <td style="width: 143px; text-align: center">
                            <asp:Label ID="lbSemestralAseg3P1" runat="server"></asp:Label>
                        </td>

                        <td style="width: 143px; text-align: center">
                            <asp:Label ID="lbSemestralAseg4P1" runat="server"></asp:Label>
                        </td>

                        <td style="width: 143px; text-align: center">
                            <asp:Label ID="lbSemestralAseg5P1" runat="server"></asp:Label>
                        </td>

                        <td style="width: 143px; text-align: center">
                            <asp:Label ID="lbSemestralAseg6P1" runat="server"></asp:Label>
                        </td>


                    </tr>

                    <tr id="SubSem" style="text-align: center">
                        <td></td>

                        <td style="width: 143px; text-align: right">
                            <asp:Label ID="lbAmpliaSEP1" runat="server" Text="Subsecuentes"></asp:Label>
                        </td>

                        <td style="width: 143px; text-align: center">
                            <asp:Label ID="lbSemestralAseg1Sub" runat="server"></asp:Label>
                        </td>

                        <td style="width: 143px; text-align: center">
                            <asp:Label ID="lbSemestralAseg2Sub" runat="server"></asp:Label>
                        </td>

                        <td style="width: 143px; text-align: center">
                            <asp:Label ID="lbSemestralAseg3Sub" runat="server"></asp:Label>
                        </td>

                        <td style="width: 143px; text-align: center">
                            <asp:Label ID="lbSemestralAseg4Sub" runat="server"></asp:Label>
                        </td>

                        <td style="width: 143px; text-align: center">
                            <asp:Label ID="lbSemestralAseg5Sub" runat="server"></asp:Label>
                        </td>

                        <td style="width: 143px; text-align: center">
                            <asp:Label ID="lbSemestralAseg6Sub" runat="server"></asp:Label>
                        </td>


                    </tr>
                </table>
                <br />
                <table id="Trimestral" style="text-align: center">
                    <tr style="height: 40px; background-color: #A9BCF5; border: 1px hidden">
                        <td></td>
                        <td style="text-align: right; width: 143px">
                            <asp:Label ID="lbTrimestral" runat="server" Text="Trimestral"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lbTrimestralAseg1" runat="server"></asp:Label>
                        </td>

                        <td>
                            <asp:Label ID="lbTrimestralAseg2" runat="server"></asp:Label>
                        </td>

                        <td>
                            <asp:Label ID="lbTrimestralAseg3" runat="server"></asp:Label>
                        </td>


                        <td>
                            <asp:Label ID="lbTrimestralAseg4" runat="server"></asp:Label>
                        </td>


                        <td>
                            <asp:Label ID="lbTrimestralAseg5" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lbTrimestralAseg6" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td style="width: 143px; text-align: right; text-size-adjust: auto">
                            <asp:Label ID="Label4" runat="server" Text="Primer pago"></asp:Label>
                        </td>

                        <td style="width: 143px; text-align: center">
                            <asp:Label ID="lbTrimestralAseg1P1" runat="server"></asp:Label>
                        </td>

                        <td style="width: 143px; text-align: center">
                            <asp:Label ID="lbTrimestralAseg2P1" runat="server"></asp:Label>
                        </td>


                        <td style="width: 143px; text-align: center">
                            <asp:Label ID="lbTrimestralAseg3P1" runat="server"></asp:Label>
                        </td>

                        <td style="width: 143px; text-align: center">
                            <asp:Label ID="lbTrimestralAseg4P1" runat="server"></asp:Label>
                        </td>

                        <td style="width: 143px; text-align: center">
                            <asp:Label ID="lbTrimestralAseg5P1" runat="server"></asp:Label>
                        </td>

                        <td style="width: 143px; text-align: center">
                            <asp:Label ID="lbTrimestralAseg6P1" runat="server"></asp:Label>
                        </td>

                    </tr>
                    <tr>
                        <td></td>
                        <td style="width: 143px; text-align: right; text-size-adjust: auto">
                            <asp:Label ID="lbTrimestralsub" runat="server" Text="Subsecuentes"></asp:Label>
                        </td>

                        <td style="width: 143px; text-align: center">
                            <asp:Label ID="lbTrimestralAseg1Sub" runat="server"></asp:Label>
                        </td>

                        <td style="width: 143px; text-align: center">
                            <asp:Label ID="lbTrimestralAseg2Sub" runat="server"></asp:Label>
                        </td>

                        <td style="width: 143px; text-align: center">
                            <asp:Label ID="lbTrimestralAseg3Sub" runat="server"></asp:Label>
                        </td>

                        <td style="width: 143px; text-align: center">
                            <asp:Label ID="lbTrimestralAseg4Sub" runat="server"></asp:Label>
                        </td>

                        <td style="width: 143px; text-align: center">
                            <asp:Label ID="lbTrimestralAseg5Sub" runat="server"></asp:Label>
                        </td>

                        <td style="width: 143px; text-align: center">
                            <asp:Label ID="lbTrimestralAseg6Sub" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
                <br />
                <table id="Mensual" style="text-align: center">
                    <tr style="height: 40px; background-color: #A9BCF5; border: 1px hidden">
                        <td></td>
                        <td style="text-align: right; width: 143px">
                            <asp:Label ID="lbMensual" runat="server" Text="Mensual"></asp:Label>
                        </td>
                        <td style="text-align: center; width: 143px">
                            <asp:Label ID="lbMensualAseg1" runat="server"></asp:Label>
                        </td>
                        <td style="text-align: center; width: 143px">
                            <asp:Label ID="lbMensualAseg2" runat="server"></asp:Label>
                        </td>
                        <td style="text-align: center; width: 143px">
                            <asp:Label ID="lbMensualAseg3" runat="server"></asp:Label>
                        </td>
                        <td style="text-align: center; width: 143px">
                            <asp:Label ID="lbMensualAseg4" runat="server"></asp:Label>
                        </td>
                        <td style="text-align: center; width: 143px">
                            <asp:Label ID="lbMensualAseg5" runat="server"></asp:Label>
                        </td>
                        <td style="text-align: center; width: 143px">
                            <asp:Label ID="lbMensualAseg6" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td style="width: 143px; text-align: right; text-size-adjust: auto">
                            <asp:Label ID="lbMensualPP" runat="server" Text="Primer pago"></asp:Label>
                        </td>

                        <td style="width: 143px; text-align: center">
                            <asp:Label ID="lbMensualAseg1P1" runat="server"></asp:Label>
                        </td>

                        <td style="width: 143px; text-align: center">
                            <asp:Label ID="lbMensualAseg2P1" runat="server"></asp:Label>
                        </td>


                        <td style="width: 143px; text-align: center">
                            <asp:Label ID="lbMensualAseg3P1" runat="server"></asp:Label>
                        </td>

                        <td style="width: 143px; text-align: center">
                            <asp:Label ID="lbMensualAseg4P1" runat="server"></asp:Label>
                        </td>

                        <td style="width: 143px; text-align: center">
                            <asp:Label ID="lbMensualAseg5P1" runat="server"></asp:Label>
                        </td>

                        <td style="width: 143px; text-align: center">
                            <asp:Label ID="lbMensualAseg6P1" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td style="width: 143px; text-align: right; text-size-adjust: auto">
                            <asp:Label ID="lbMensualSub" runat="server" Text="Subsecuentes"></asp:Label>
                        </td>

                        <td style="width: 143px; text-align: center">
                            <asp:Label ID="lbMensualAseg1Sub" runat="server"></asp:Label>
                        </td>

                        <td style="width: 143px; text-align: center">
                            <asp:Label ID="lbMensualAseg2Sub" runat="server"></asp:Label>
                        </td>

                        <td style="width: 143px; text-align: center">
                            <asp:Label ID="lbMensualAseg3Sub" runat="server"></asp:Label>
                        </td>

                        <td style="width: 143px; text-align: center">
                            <asp:Label ID="lbMensualAseg4Sub" runat="server"></asp:Label>
                        </td>

                        <td style="width: 143px; text-align: center">
                            <asp:Label ID="lbMensualAseg5Sub" runat="server"></asp:Label>
                        </td>

                        <td style="width: 143px; text-align: center">
                            <asp:Label ID="lbMensualAseg6Sub" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
                <br />
            </div>
            <br />
            <div>
                <%--  <asp:RadioButtonList ID="RadioButtonList1" runat="server" Width="800"  RepeatDirection="Horizontal" >
                <asp:ListItem Text ="Item1" Value="1" />
                <asp:ListItem Text ="Item2" Value="2" />
                <asp:ListItem Text ="Item3" Value="3" />
                <asp:ListItem Text ="Item4" Value="4" />--%>

                <%--  </asp:RadioButtonList>--%>

                <%--  <asp:CheckBox ID="CheckBox1" runat="server"  Text="ANA" ValidationGroup="Asgs"/>
                 <asp:CheckBox ID="CheckBox2" runat="server" Text="ABA" ValidationGroup="Asgs"/>
                 <asp:CheckBox ID="CheckBox3" runat="server" Text="AXA" ValidationGroup="Asgs"/>
                 <asp:CheckBox ID="CheckBox4" runat="server" Text="ZURICH" ValidationGroup="Asgs"/>--%>
            </div>

            <div>

                <asp:Button ID="btnAmpliaEP" runat="server" Text="Plus" CssClass="btn btn-primary" Width="200px"  OnClick="btnAmpliaEP_Click" />
                &nbsp;&nbsp;&nbsp;
                <asp:Button ID="btnAmplia" runat="server" Text="Amplia" CssClass="btn btn-primary" Width="200px" OnClick="btnAmplia_Click" />
                &nbsp;&nbsp;&nbsp;
                <asp:Button ID="btnLimitadaEP" runat="server" Text="Limitada" CssClass="btn btn-primary" Width="200px" OnClick="btnLimitadaEP_Click" />
                <%--<asp:Button ID="btnLimitada" runat="server" Text="Limitada"  Width="100px" OnClick="btnLimitada_Click" />--%>
            </div>
                <div>

               
              <%--  <asp:Button ID="Button4" runat="server" Text="EnviarCorreo" OnClick="Button4_Click"/>--%>
                <%--<asp:Button ID="btnLimitada" runat="server" Text="Limitada"  Width="100px" OnClick="btnLimitada_Click" />--%>
            </div>
            <br />
            <table style="margin: auto">
                <tr>
                    <td>
                        <asp:GridView ID="gwCoberturas" runat="server"  Width="1000px"></asp:GridView>
                    </td>
                </tr>
            </table>
            <br />
            <div>

                <asp:Button ID="Button1" runat="server"  Text="Contratar" OnClick="Button1_Click" />

            </div>


        </div>


    </div>

   
</asp:Content>
