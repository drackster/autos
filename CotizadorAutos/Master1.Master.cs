﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CotizadorAutos
{
    public partial class Master1 : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           if(Session["idUsuario"].ToString() == "0")
           {
               btnCotizaciones.Visible = false;
               btnSalir.Visible = false;
           }
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("http://estrategiaenproteccion.com/aviso-de-privacidad/");
        }

        protected void btLogin_ServerClick(object sender, EventArgs e)
        {       


           
        }

        protected void btnReg_ServerClick(object sender, EventArgs e)
        {
            Response.Redirect("RegistroUsuario.aspx");
        }

        protected void btnCotizaciones_Click(object sender, EventArgs e)
        {
            Response.Redirect("ReporteCotizaciones.aspx");
        }

        protected void btnSalir_Click(object sender, EventArgs e)
        {
            Session.Clear();
            Response.Redirect("DatosCliente.aspx");
        }

       
    }
}