﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace CotizadorAutos
{
    public class AccesoDatos
    {
        string connectionString = ConfigurationManager.ConnectionStrings["ConexionWindows"].ConnectionString;
        public static DataTable fnObtenerPrimas()
        {
            var Primas = new DataTable();
            var conexion = new SqlConnection();
            conexion.ConnectionString = ConfigurationManager.ConnectionStrings["ConexionWindows"].ConnectionString;
            {
                conexion.Open();
                using (SqlDataAdapter da = new SqlDataAdapter("spObtenerPrimaAba", conexion))
                {
                    da.Fill(Primas);
                    conexion.Close();
                }
            }
            return Primas;
        }

       
        public static DataSet fnObtenerMarcas(int subramo)
        {

            var dsMarcas = new DataSet();
            var conexion = new SqlConnection();
            conexion.ConnectionString = ConfigurationManager.ConnectionStrings["ConexionWindows"].ConnectionString;
            {
                conexion.Open();
                using (SqlDataAdapter da = new SqlDataAdapter("spObtenerMarcaAna ", conexion))
                {		

                    da.Fill(dsMarcas);
                    conexion.Close();
                }


            }
            return dsMarcas;
        }

      

        public static DataTable fnObtenerSubmarca(string marca)
        {
            var dtSubMarcas = new DataTable();
            var conexion = new SqlConnection();
            SqlDataAdapter da = new SqlDataAdapter();
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConexionWindows"].ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("spObtenerSubmarcaAna", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Marca", marca);
                da.SelectCommand = cmd;
                con.Open();
                da.Fill(dtSubMarcas);
                con.Close();
            }
            return dtSubMarcas;
        }

      

        public static DataTable fnObtenerAño(string marca, string submarca)
        {
            var iAño = new DataTable();
            var conexion = new SqlConnection();
            SqlDataAdapter da = new SqlDataAdapter();
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConexionWindows"].ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("spObtenerModeloAna", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Marca", marca);
                cmd.Parameters.AddWithValue("@Submarca", submarca);
                da.SelectCommand = cmd;
                con.Open();
                da.Fill(iAño);
                con.Close();
            }
            return iAño;
        }

        
        public static DataTable fnObtenerDescripcion(string marca, string submarca, string modelo)
        {
            var Descripcion = new DataTable();
            var conexion = new SqlConnection();
            SqlDataAdapter da = new SqlDataAdapter();
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConexionWindows"].ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("spObtenerDescripcionAna", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Marca", marca);
                cmd.Parameters.AddWithValue("@Submarca", submarca);
                cmd.Parameters.AddWithValue("@Modelo", modelo);
                da.SelectCommand = cmd;
                con.Open();
                da.Fill(Descripcion);
                con.Close();
            }
            return Descripcion;
        }

        public static DataTable fnObtenerAmisAna(string marca, string submarca, string modelo, string descripcion)
        {
            DataTable ClavesVehiculares = new DataTable();
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConexionWindows"].ConnectionString))
            {
                SqlCommand cmd = new SqlCommand("spObtenerClaveVehicular", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Marca", marca);
                cmd.Parameters.AddWithValue("@Submarca", submarca);
                cmd.Parameters.AddWithValue("@Modelo", modelo);
                cmd.Parameters.AddWithValue("@Descripcion", descripcion);

             

                using (SqlDataAdapter a = new SqlDataAdapter(cmd))
                {
                    con.Open();
                    a.Fill(ClavesVehiculares);
                    con.Close();
                }

            }
       
            return ClavesVehiculares;
        }

        public static DataTable fnObtenerEstados()
        {
            var Descripcion = new DataTable();
            var conexion = new SqlConnection();
            conexion.ConnectionString = ConfigurationManager.ConnectionStrings["ConexionWindows"].ConnectionString;
            {
                conexion.Open();
                using (SqlDataAdapter da = new SqlDataAdapter("spObtenerEstados", conexion))
                {
                    da.Fill(Descripcion);
                    conexion.Close();
                }
            }
            return Descripcion;
        }
        public static DataTable fnObtenerMunicipios(int iEstado)
        {
            var Descripcion = new DataTable();
            var conexion = new SqlConnection();
            conexion.ConnectionString = ConfigurationManager.ConnectionStrings["ConexionWindows"].ConnectionString;
            {
                conexion.Open();
                using (SqlDataAdapter da = new SqlDataAdapter("spObtenerMunicipios " + iEstado + "", conexion))
                {
                    da.Fill(Descripcion);
                    conexion.Close();
                }
            }
            return Descripcion;
        }
        public static DataTable fnObtenerColonias(int iMunicipio)
        {
            var Colonias = new DataTable();
            var conexion = new SqlConnection();
            conexion.ConnectionString = ConfigurationManager.ConnectionStrings["ConexionWindows"].ConnectionString;
            {
                conexion.Open();
                using (SqlDataAdapter da = new SqlDataAdapter("spObtenerColonias " + iMunicipio, conexion))
                {
                    da.Fill(Colonias);
                    conexion.Close();
                }
            }
            return Colonias;

        }
        public static DataSet fnObtenerCodigosPostales(int idCodigoPostal)
        {
            var CodigosPostales = new DataSet();
            var conexion = new SqlConnection();
            conexion.ConnectionString = ConfigurationManager.ConnectionStrings["ConexionWindows"].ConnectionString;
            {
                conexion.Open();
                using (SqlDataAdapter da = new SqlDataAdapter("spObtenerCodigoPostal " + idCodigoPostal, conexion))
                {
                    da.Fill(CodigosPostales);
                    conexion.Close();
                }
            }
            return CodigosPostales;
        }
        public static DataSet fnObtenerColoniaMunicipioEstado(string CodigoPostal)
        {
            var dsUbicacion = new DataSet();
            var conexion = new SqlConnection();
            conexion.ConnectionString = ConfigurationManager.ConnectionStrings["ConexionWindows"].ConnectionString;
            {
                conexion.Open();
                using (SqlDataAdapter da = new SqlDataAdapter("spObtenerUbicacionCP " + "'" + CodigoPostal + "'", conexion))
                {
                    da.Fill(dsUbicacion);
                    conexion.Close();
                }
            }
            return dsUbicacion;
        }
        public static DataSet fnObtenerNumeroDeCotizacion()
        {
            var NoCotizacion = new DataSet();
            var conexion = new SqlConnection();
            conexion.ConnectionString = ConfigurationManager.ConnectionStrings["ConexionWindows"].ConnectionString;
            {
                conexion.Open();
                using (SqlDataAdapter da = new SqlDataAdapter("SELECT MAX(idCotizacion) as idCotizacion FROM tCotizaciones", conexion))
                {
                    da.Fill(NoCotizacion);
                    conexion.Close();
                }
            }
            return NoCotizacion;
        }
        public static void fnIsertaDatosliente(
                                           int idUsuario,
                                        string Nombre,
                                        string Apellido,
                                        string Estado,
                                        string Municipio,
                                        string Colonia,
                                        string CodigoPostal,
                                        string Telefono,
                                        string CorreoElectronico,
                                        string Marca,
                                        string Submarca,
                                        string Modelo,
                                        string descripcion)
        {
            var conexion = new SqlConnection();
            conexion.ConnectionString = ConfigurationManager.ConnectionStrings["ConexionWindows"].ConnectionString;
            conexion.Open();
            using (SqlCommand insert = new SqlCommand("spInsertaDatosCotizacion " + idUsuario + ",'" + Nombre + "'," + "'" + Apellido + "'," + "'" + Estado + "'," + "'" + Municipio + "'," + "'" + Colonia + "'," + "'" + CodigoPostal + "'," + "'" + Telefono + "'," + "'" + CorreoElectronico + "','"
                                                                                  + Marca + "'" + ",'" + Submarca + "'" + ",'" + Modelo + "'" + ",'" + descripcion + "'", conexion))
            {
                insert.ExecuteReader();
                conexion.Close();
            }

        }

        public static DataSet fnLogin(string Usuario, string Contraseña)
        {
            var DatosUsuario = new DataSet();
            var conexion = new SqlConnection();

            conexion.ConnectionString = ConfigurationManager.ConnectionStrings["ConexionWindows"].ConnectionString;
            {
                conexion.Open();
                using (SqlDataAdapter da = new SqlDataAdapter("spLogin 1, " + Usuario + "," + Contraseña, conexion))
                {
                    da.Fill(DatosUsuario);
                    conexion.Close();
                }
            }
            return DatosUsuario;

        }


        public static int fnVerificaUsuario(string Usuario)
        {
            var conexion = new SqlConnection();
            Int32 existe = 0;
            var DatosUsuario = new DataSet();
            conexion.ConnectionString = ConfigurationManager.ConnectionStrings["ConexionWindows"].ConnectionString;
            {
                conexion.Open();
                using (SqlCommand select = new SqlCommand("spLogin 2," + Usuario + "," + "''", conexion))
                {
                    SqlDataReader reader = select.ExecuteReader();
                    while (reader.Read())
                    {
                        existe = reader.GetInt32(0);
                    }
                    conexion.Close();
                }
            }
            return existe;
        }

        public static int fnInsertaConstraseña(string Usuario, string Contraseña)
        {

            var conexion = new SqlConnection();
            Int32 existe = 0;
            conexion.ConnectionString = ConfigurationManager.ConnectionStrings["ConexionWindows"].ConnectionString;
            {
                conexion.Open();
                using (SqlCommand select = new SqlCommand("spLogin 3," + Usuario + "," + Contraseña, conexion))
                {
                    SqlDataReader reader = select.ExecuteReader();
                    while (reader.Read())
                    {
                        existe = reader.GetInt32(0);
                    }
                    conexion.Close();
                }
            }
            return existe;
        }


        public static DataSet fnObtenerCotizacionesPorUsuario(int idusuario)
        {
            var Cotizaciones = new DataSet();
            var conexion = new SqlConnection();
            conexion.ConnectionString = ConfigurationManager.ConnectionStrings["ConexionWindows"].ConnectionString;
            {
                conexion.Open();
                using (SqlDataAdapter da = new SqlDataAdapter("Select * from tCotizaciones where idusuario = " + idusuario + "", conexion))
                {
                    da.Fill(Cotizaciones);
                    conexion.Close();
                }
            }
            return Cotizaciones;
        }

        public static DataSet fnObtenerMarcas2()
        {
            DataSet dt = new DataSet();
            using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConexionWindows"].ConnectionString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("spSeleccionaVehiculoAna", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@iOpcion", 1);
                cmd.Parameters.AddWithValue("@sMarca", "");
                cmd.Parameters.AddWithValue("@sSubmarca", "");
                cmd.Parameters.AddWithValue("@sModelo", "");
                cmd.Parameters.AddWithValue("@sDescripcion", "");
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                cn.Close();
            }
            return dt;
        }

        public static DataSet ConsultaCatlagogoAna()
        {
            DataSet ds = new DataSet();

            using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConexionWindows"].ConnectionString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("spConsultaCatalogoAna", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@iOpcion", 1);
                cmd.Parameters.AddWithValue("@Marca", "");
                cmd.Parameters.AddWithValue("@Submarca", "");
                cmd.Parameters.AddWithValue("@Modelo", "");
                cmd.Parameters.AddWithValue("@Descripcio", "");

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
                cn.Close();
            }


            return ds;
        }

        public static DataTable ConsultaCatlagogoAnaDescripcion(string marca)
        {
            DataTable ds = new DataTable();

            using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConexionWindows"].ConnectionString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("spConsultaCatalogoAna", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@iOpcion", 2);
                cmd.Parameters.AddWithValue("@Marca", marca);
                cmd.Parameters.AddWithValue("@Submarca", "");
                cmd.Parameters.AddWithValue("@Modelo", "");
                cmd.Parameters.AddWithValue("@Descripcio", "");

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
                cn.Close();
            }


            return ds;
        }

        public static DataTable ConsultaCatlagogoAnaModelo(string marca, string descripcion)
        {
            DataTable ds = new DataTable();

            using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConexionWindows"].ConnectionString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("spConsultaCatalogoAna", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@iOpcion", 3);
                cmd.Parameters.AddWithValue("@Marca", marca);
                cmd.Parameters.AddWithValue("@Submarca", descripcion);
                cmd.Parameters.AddWithValue("@Modelo", "");
                cmd.Parameters.AddWithValue("@Descripcio", "");

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds);
                cn.Close();
            }


            return ds;
        }

        public static Int32 ObtenerIdEstadoMunAna(int idEstado, string IdMun)
        {
            Int32 id_estado = 0;

            using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConexionWindows"].ConnectionString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("soObteneridCiudadAna", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@iEstado", idEstado);
                cmd.Parameters.AddWithValue("@sMunicipio", IdMun);

                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    id_estado = reader.GetInt32(0);
                }
                cn.Close();
            }
            return id_estado;
        }

        public static DataTable FnObtenerSeguradorasActivas()
        {
            DataTable dt = new DataTable();

            using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConexionWindows"].ConnectionString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("SpObtenerAseguradorasActivas", cn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                cn.Close();
                
            }

            return dt;
        }

        public static DataTable FnObtenerCoberturas(int Aseg, int Paq)
        {
            DataTable dt = new DataTable();

            using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConexionWindows"].ConnectionString))
            {
                cn.Open();
                SqlCommand cmd = new SqlCommand("spObtenerCoberturasPorAseguradora", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@idAsguradora", Aseg);
                cmd.Parameters.AddWithValue("@idPaquete", Paq);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                cn.Close();
            }
            return dt;
        }
    }
}
