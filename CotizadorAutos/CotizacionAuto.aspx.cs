﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace CotizadorAutos
{

    public partial class CotizacionAuto : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                Session.Add("TipoCotizacion", "");
                fnObtenerAseguradoras(0);

            }
            else
            {
                lblMarca.Text = Session["Marca"].ToString();
                lblsubmarca.Text = Session["Submarca"].ToString();
                lblaño.Text = Session["Modelo"].ToString();
                lblVersion.Text = Session["Descripcion"].ToString();
                lblCotizacion1.Text = Session["NoCotizacion"].ToString();

            }
        }

        private DataSet fnLlenaGridCoberturas(int paquete)
        {
            var Coberturas = new DataSet();
            var conexion = new SqlConnection();
            conexion.ConnectionString = ConfigurationManager.ConnectionStrings["ConexionWindows"].ConnectionString;
            {
                conexion.Open();
                using (SqlDataAdapter da = new SqlDataAdapter("spObtenerPaquetes" + " " + paquete, conexion))
                {
                    da.Fill(Coberturas);
                    conexion.Close();
                }
            }
            return Coberturas;
        }

        public string[] fnObtenerAseguradoras(int idVehiculoHomologado)
        {
            string[] array = { "1", "2", "3", "4", "5", "6" };
            cotiza(array, 0);
            return array;
        }

        public void cotiza(string[] Aseguradoras, int iPaquete)
        {
            string primas = "";
            foreach (string Aseg in Aseguradoras)
            {
                switch (Aseg)
                {
                    case "1":
                        {
                            #region ana
                            //DataTable PrimasNetas = new DataTable();
                            //PrimasNetas = AccesoDatos.fnObtenerPrimas();
                            //primas = PrimasNetas.Rows[0]["Prima"].ToString();

                            string[] num = CotizaAna(iPaquete);


                            primas = "4000";//num[1];
                            ImgAseg1.ImageUrl = "Images/ana.jpg";
                            /*Contado*/
                            double PrimaNeta = double.Parse(primas);

                            double DerechoPoliza = 480;// double.Parse(num[3]);
                            double contado = PrimaNeta + DerechoPoliza;
                            double result = contado * 1.16;
                            lbContadoAseg1.Text = result.ToString("C");

                            /*Semestral*/

                            double RecargoSem = 1.044;

                            double Semestral = (((PrimaNeta * RecargoSem) + DerechoPoliza) * 1.16);
                            double SemP1 = (((Semestral) + (DerechoPoliza * 1.16)) / 2);
                            double SemSub = (((Semestral) - (DerechoPoliza * 1.16)) / 2);

                            lbSemestralAseg1.Text = Semestral.ToString("C");
                            lbSemestralAseg1P1.Text = SemP1.ToString("C");
                            lbSemestralAseg1Sub.Text = SemSub.ToString("C");

                            /*trimestral*/

                            double Recargotri = 1.068;

                            double trimestral = (((PrimaNeta * Recargotri) + DerechoPoliza) * 1.16);
                            double triSub = (((trimestral) - (DerechoPoliza * 1.16)) / 4);
                            double triP1 = triSub + (DerechoPoliza * 1.16);


                            lbTrimestralAseg1.Text = trimestral.ToString("C");
                            lbTrimestralAseg1P1.Text = triP1.ToString("C");
                            lbTrimestralAseg1Sub.Text = triSub.ToString("C");

                            /*mensual*/

                            double RecargoMen = 1.084;

                            double Mensual = (((PrimaNeta * RecargoMen) + DerechoPoliza) * 1.16);
                            //double MenSub = (((Mensual) - (DerechoPoliza * 1.16)) / 12);  cálculo con pago de derecho de póliza en el primer recibo
                            //double MenP1 = MenSub + (DerechoPoliza * 1.16);

                            double MenSub = (Mensual / 12);
                            double MenP1 = (Mensual / 12);

                            lbMensualAseg1.Text = Mensual.ToString("C");
                            lbMensualAseg1P1.Text = MenP1.ToString("C");
                            lbMensualAseg1Sub.Text = MenSub.ToString("C");

                            Session["PrecioContado"] = result.ToString("C");
                            Session["PrecioSemestral"] = Semestral.ToString("C");
                            Session["PrecioTrimestral"] = trimestral.ToString("C");
                            Session["PrecioMensual"] = Mensual.ToString("C");

                            Session["PrecioSem1"] = SemP1.ToString("C"); ;
                            Session["PrecioSemSub"] = SemSub.ToString("C"); ;

                            Session["PrecioTri1"] = triP1.ToString("C"); ;
                            Session["PrecioTriSub"] = triSub.ToString("C"); ;

                            Session["PrecioMen1"] = MenP1.ToString("C"); ;
                            Session["PrecioMenSub"] = MenSub.ToString("C"); ;


                            #endregion
                        }
                        break;

                    case "2":
                        {
                            #region aba
                            DataTable PrimasNetas = new DataTable();
                            PrimasNetas = AccesoDatos.fnObtenerPrimas();
                            primas = "4200";// PrimasNetas.Rows[0]["Prima"].ToString();
                            primas.Split('|');
                            ImgAseg2.ImageUrl = "Images/aba.jpg";

                            // Instanciamos el objeto proxy de conexion al WCF
                            double PrimaCotAba = CotizaAba(iPaquete);

                            /*Contado*/
                            double recargo = 1.05;
                            double DerechoPoliza = 400;
                            double contado = PrimaCotAba;//Convert.ToDouble(primas.Split('|')[iPaquete].ToString());
                            double result = (contado + DerechoPoliza) * 1.16;
                            lbContadoAseg2.Text = result.ToString("C");

                            /*Semestral*/
                            //contado = Convert.ToDouble(primas.Split('|')[iPaquete].ToString());
                            double recargoSem = 1.05;
                            double Semestral = (((contado + DerechoPoliza) * recargoSem) * 1.16);
                            double SemP1 = ((((contado / 2) + DerechoPoliza) * recargoSem) * 1.16);
                            double SemSub = (((contado / 2) * recargo) * 1.16);
                            lbSemestralAseg2.Text = Semestral.ToString("C");
                            lbSemestralAseg2P1.Text = SemP1.ToString("C");
                            lbSemestralAseg2Sub.Text = SemSub.ToString("C");


                            /*trimestral*/
                            //contado = Convert.ToDouble(primas.Split('|')[iPaquete].ToString());  
                            double recargoTri = 1.08;
                            double trimestral = (((contado + DerechoPoliza) * recargoTri) * 1.16);
                            double triP1 = ((((contado / 4) + DerechoPoliza) * recargoTri) * 1.16);
                            double triSub = (((contado / 4) * recargo) * 1.16);
                            lbTrimestralAseg2.Text = trimestral.ToString("C");
                            lbTrimestralAseg2P1.Text = triP1.ToString("C");
                            lbTrimestralAseg2Sub.Text = triSub.ToString("C");


                            /*mensual*/
                            double recargoMen = 1.1;
                            double Mensual = (((contado + DerechoPoliza) * recargoMen) * 1.16);
                            double MenP1 = ((((contado / 12) + DerechoPoliza) * recargoMen) * 1.16);
                            double MenSub = (((contado / 12) * recargo) * 1.16);
                            lbMensualAseg2.Text = Mensual.ToString("C");
                            lbMensualAseg2P1.Text = MenP1.ToString("C");
                            lbMensualAseg2Sub.Text = MenSub.ToString("C");



                            #endregion
                        }
                        break;

                    case "3":
                        {
                            #region zurich
                            DataTable PrimasNetas = new DataTable();
                            PrimasNetas = AccesoDatos.fnObtenerPrimas();
                            primas = "4350";//PrimasNetas.Rows[0]["Prima"].ToString();
                            primas.Split('|');
                            ImgAseg3.ImageUrl = "Images/zurich.jpg";

                            /*Contado*/
                            double recargo = 1.05;
                            double DerechoPoliza = 400;
                            double contado = Convert.ToDouble(primas.Split('|')[iPaquete].ToString());
                            double result = (contado + DerechoPoliza) * 1.16;
                            lbContadoAseg3.Text = result.ToString("C");

                            /*Semestral*/
                            double recargoSem = 1.08;
                            double Semestral;
                            Semestral = (((contado + DerechoPoliza) * recargoSem) * 1.16);
                            double SemP1 = ((((contado / 2) + DerechoPoliza) * recargoSem / 2) * 1.16);
                            double SemSub = (((contado / 2) * recargoSem / 2) * 1.16);
                            lbSemestralAseg3.Text = Semestral.ToString("C");
                            lbSemestralAseg3P1.Text = SemP1.ToString("C");
                            lbSemestralAseg3Sub.Text = SemSub.ToString("C");

                            /*trimestral*/
                            double recargoTri = 1.09;
                            double Trimestarl = (((contado + DerechoPoliza) * recargoTri) * 1.16);
                            double TriP1 = ((((contado / 4) + DerechoPoliza) * recargoTri / 4) * 1.16);
                            double TriSub = (((contado / 4) * recargoTri / 4) * 1.16);
                            lbTrimestralAseg3.Text = Trimestarl.ToString("C");
                            lbTrimestralAseg3P1.Text = TriP1.ToString("C");
                            lbTrimestralAseg3Sub.Text = TriSub.ToString("C");

                            /*mensual*/
                            double recargoMen = 1.1;
                            double Mensual = (((contado + DerechoPoliza) * recargoMen) * 1.16);
                            double MenP1 = ((((contado / 12) + DerechoPoliza) * recargoMen / 12) * 1.16);
                            double MenSub = (((contado / 12) * recargoMen / 12) * 1.16);
                            lbMensualAseg3.Text = Mensual.ToString("C");
                            lbMensualAseg3P1.Text = MenP1.ToString("C");
                            lbMensualAseg3Sub.Text = MenSub.ToString("C");
                            #endregion
                        }
                        break;

                    case "4":
                        {
                            #region mapfre
                            DataTable PrimasNetas = new DataTable();
                            PrimasNetas = AccesoDatos.fnObtenerPrimas();
                            primas = "3950";//PrimasNetas.Rows[0]["Prima"].ToString();
                            primas.Split('|');
                            ImgAseg4.ImageUrl = "Images/mapfre.jpg";

                            /*Contado*/
                            double recargo = 1.05;
                            double DerechoPoliza = 400;
                            double contado = Convert.ToDouble(primas.Split('|')[iPaquete].ToString());
                            double result = (contado + DerechoPoliza) * 1.16;
                            lbContadoAseg4.Text = result.ToString("C");

                            /*Semestral*/
                            double Semestral;
                            Semestral = (((contado + DerechoPoliza) * recargo) * 1.16);
                            double SemP1 = ((((contado / 2) + DerechoPoliza) * recargo) * 1.16);
                            double SemSub = (((contado / 2) * recargo) * 1.16);
                            lbSemestralAseg4.Text = Semestral.ToString("C");
                            lbSemestralAseg4P1.Text = SemP1.ToString("C");
                            lbSemestralAseg4Sub.Text = SemSub.ToString("C");

                            /*Trimestral*/
                            double recargoTri = 1.09;
                            double Trimestarl = (((contado + DerechoPoliza) * recargoTri) * 1.16);
                            double TriP1 = ((((contado / 4) + DerechoPoliza) * recargoTri / 4) * 1.16);
                            double TriSub = (((contado / 4) * recargoTri / 4) * 1.16);
                            lbTrimestralAseg4.Text = Trimestarl.ToString("C");
                            lbTrimestralAseg4P1.Text = TriP1.ToString("C");
                            lbTrimestralAseg4Sub.Text = TriSub.ToString("C");

                            /*Mensual*/
                            double recargoMen = 1.1;
                            double Mensual = (((contado + DerechoPoliza) * recargoMen) * 1.16);
                            double MenP1 = ((((contado / 12) + DerechoPoliza) * recargoMen / 12) * 1.16);
                            double MenSub = (((contado / 12) * recargoMen / 12) * 1.16);
                            lbMensualAseg4.Text = Mensual.ToString("C");
                            lbMensualAseg4P1.Text = MenP1.ToString("C");
                            lbMensualAseg4Sub.Text = MenSub.ToString("C");
                            #endregion

                        }
                        break;

                    case "5":
                        {
                            #region axa
                            DataTable PrimasNetas = new DataTable();
                            PrimasNetas = AccesoDatos.fnObtenerPrimas();
                            primas = "5000";// PrimasNetas.Rows[0]["Prima"].ToString();
                            primas.Split('|');
                            ImgAseg5.ImageUrl = "Images/axa.jpg";

                            /*Contado*/
                            double recargo = 1.05;
                            double DerechoPoliza = 400;
                            double contado = Convert.ToDouble(primas.Split('|')[iPaquete].ToString());
                            double result = (contado + DerechoPoliza) * 1.16;
                            lbContadoAseg5.Text = result.ToString("C");

                            /*Semestral*/
                            contado = Convert.ToDouble(primas.Split('|')[iPaquete].ToString());
                            double Semestral;
                            Semestral = (((contado + DerechoPoliza) * recargo) * 1.16);
                            double SemP1 = ((((contado / 2) + DerechoPoliza) * recargo) * 1.16);
                            double SemSub = (((contado / 2) * recargo) * 1.16);
                            lbSemestralAseg5.Text = Semestral.ToString("C");
                            lbSemestralAseg5P1.Text = SemP1.ToString("C");
                            lbSemestralAseg5Sub.Text = SemSub.ToString("C");

                            /*Trimestral*/
                            double recargoTri = 1.09;
                            double Trimestarl = (((contado + DerechoPoliza) * recargoTri) * 1.16);
                            double TriP1 = ((((contado / 4) + DerechoPoliza) * recargoTri / 4) * 1.16);
                            double TriSub = (((contado / 4) * recargoTri / 4) * 1.16);
                            lbTrimestralAseg5.Text = Trimestarl.ToString("C");
                            lbTrimestralAseg5P1.Text = TriP1.ToString("C");
                            lbTrimestralAseg5Sub.Text = TriSub.ToString("C");

                            /*Mensual*/
                            double recargoMen = 1.1;
                            double Mensual = (((contado + DerechoPoliza) * recargoMen) * 1.16);
                            double MenP1 = ((((contado / 12) + DerechoPoliza) * recargoMen / 12) * 1.16);
                            double MenSub = (((contado / 12) * recargoMen / 12) * 1.16);
                            lbMensualAseg5.Text = Mensual.ToString("C");
                            lbMensualAseg5P1.Text = MenP1.ToString("C");
                            lbMensualAseg5Sub.Text = MenSub.ToString("C");
                            #endregion
                        }
                        break;

                    case "6":
                        {
                            #region rsa
                            DataTable PrimasNetas = new DataTable();
                            PrimasNetas = AccesoDatos.fnObtenerPrimas();
                            primas = "6500";//PrimasNetas.Rows[0]["Prima"].ToString();
                            primas.Split('|');
                            ImgAseg6.ImageUrl = "Images/rsa.jpg";


                            /*Contado*/
                            double recargo = 1.05;
                            double DerechoPoliza = 400;
                            double contado = Convert.ToDouble(primas.Split('|')[iPaquete].ToString());
                            double result = (contado + DerechoPoliza) * 1.16;
                            lbContadoAseg6.Text = result.ToString("C");

                            /*Semestral*/
                            contado = Convert.ToDouble(primas.Split('|')[iPaquete].ToString());
                            double Semestral;
                            Semestral = (((contado + DerechoPoliza) * recargo) * 1.16);
                            double SemP1 = ((((contado / 2) + DerechoPoliza) * recargo) * 1.16);
                            double SemSub = (((contado / 2) * recargo) * 1.16);
                            lbSemestralAseg6.Text = Semestral.ToString("C");
                            lbSemestralAseg6P1.Text = SemP1.ToString("C");
                            lbSemestralAseg6Sub.Text = SemSub.ToString("C");

                            /*Trimestral*/
                            double recargoTri = 1.09;
                            double Trimestarl = (((contado + DerechoPoliza) * recargoTri) * 1.16);
                            double TriP1 = ((((contado / 4) + DerechoPoliza) * recargoTri / 4) * 1.16);
                            double TriSub = (((contado / 4) * recargoTri / 4) * 1.16);
                            lbTrimestralAseg6.Text = Trimestarl.ToString("C");
                            lbTrimestralAseg6P1.Text = TriP1.ToString("C");
                            lbTrimestralAseg6Sub.Text = TriSub.ToString("C");

                            /*Mensual*/
                            double recargoMen = 1.1;
                            double Mensual = (((contado + DerechoPoliza) * recargoMen) * 1.16);
                            double MenP1 = ((((contado / 12) + DerechoPoliza) * recargoMen / 12) * 1.16);
                            double MenSub = (((contado / 12) * recargoMen / 12) * 1.16);
                            lbMensualAseg6.Text = Mensual.ToString("C");
                            lbMensualAseg6P1.Text = MenP1.ToString("C");
                            lbMensualAseg6Sub.Text = MenSub.ToString("C");

                            #endregion
                        }
                        break;

                }


            }

        }

        protected void btnAmpliaEP_Click(object sender, EventArgs e)
        {
            divPrimas.Visible = true;
            Session["TipoCotizacion"] = "Amplia EP";
            string[] array = { "1"/*,  "2", "3", "4", "5", "6"*/ };
            cotiza(array, 0);

            //var coberturas = fnLlenaGridCoberturas(1);
            //gwCoberturas.DataSource = coberturas;
            //gwCoberturas.DataBind();
        }

        protected void btnAmplia_Click(object sender, EventArgs e)
        {
            divPrimas.Visible = true;
            Session["TipoCotizacion"] = "Amplia";
            string[] array = { "1"/*,  "2", "3", "4", "5", "6"*/ };
            cotiza(array, 1);

            //var coberturas = fnLlenaGridCoberturas(2);
            //gwCoberturas.DataSource = coberturas;
            //gwCoberturas.DataBind();

        }

        protected void btnLimitadaEP_Click(object sender, EventArgs e)
        {
            divPrimas.Visible = true;
            Session["TipoCotizacion"] = "Limitada";
            string[] array = { "1", "2"/* "3", "4", "5", "6" */};
            cotiza(array, 2);

            //var coberturas = fnLlenaGridCoberturas(3);
            //gwCoberturas.DataSource = coberturas;
            //gwCoberturas.DataBind();
        }

        protected void btnLimitada_Click(object sender, EventArgs e)
        {
            divPrimas.Visible = true;
            string[] array = { "1", "2"/* "3", "4", "5", "6" */ };
            cotiza(array, 3);


            //var coberturas = fnLlenaGridCoberturas(4);
            //gwCoberturas.DataSource = coberturas;
            //gwCoberturas.DataBind();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/contrata.aspx");
        }

        protected string[] CotizaAna(int paquete)
        {

            DataTable amis = Session["AmisAna"] as DataTable;

            string clave =  amis.Rows[0]["claveAna"].ToString();

            string[] num = { "" };
            string cot = string.Empty;
            var doc = new XmlDocument();
            string xmlAna = "";
            string root = "";
            string AmisAna = clave;
            string Modelo = Session["Modelo"].ToString();
            int iEstadoAna = int.Parse(Session["id_Estado"].ToString());
            string Mun = Session["Municipio"].ToString();
            string Rem = remplazar(Mun);
            string concatC;
            string concatE;
            // 01 estado 009 municipio 
            Int32 id_ciudad = AccesoDatos.ObtenerIdEstadoMunAna(iEstadoAna, Rem);

            if (id_ciudad < 10)
            {
                concatC = "00" + id_ciudad.ToString();
            }
            else if (id_ciudad > 9 && id_ciudad < 100)
            {
                concatC = "0" + id_ciudad.ToString();
            }
            else
            {
                concatC = id_ciudad.ToString();
            }

            if (iEstadoAna < 10)
            {
                concatE = "0" + iEstadoAna.ToString();
            }
            else
            {
                concatE = iEstadoAna.ToString();
            }

            string id_Ciudad = concatE + concatC;

            string FechaInicioVigencia = DateTime.Today.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            string FechaTerminoVigencia = DateTime.Today.AddYears(1).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            string FechaEmision = DateTime.Today.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            if (paquete == 0)
            {
                root = Server.MapPath("~/Xml/AnaAmPlus.xml");
                xmlAna = "<transacciones xmlns=\"\">"
                   + "<transaccion tiponegocio=\"\" negocio=\"640\" cotizacion=\"\" tipotransaccion=\"C\" version=\"1\">"
                   + "<vehiculo tipocargadescripcion=\"\" tipocarga=\"\" fecdispositivo=\"\" dispositivo=\"\" color=\"01\" poblacion=\"" + Rem + "\" estado=\"" + id_Ciudad + "\" conductorocupacion=\"\" conductorfecnac=\"\" conductorliciencia=\"\" conductor=\"\" placas=\"\" repuve=\"\" serie=\"\" motor=\"\" plan=\"1\" servicio=\"1\" uso=\"1\" descripcion=\"\" modelo=\"" + Modelo + "\" amis=\"" + AmisAna + "\" id=\"1\">"
                   + "<cobertura id=\"02\" pma=\"\" ded=\"3\" tipo=\"1\" sa=\"\" desc=\"\" />"
                   + "<cobertura id=\"04\" pma=\"\" ded=\"5\" tipo=\"1\" sa=\"\" desc=\"\" />"
                   + "<cobertura id=\"05\" pma=\"\" ded=\"\" tipo=\"\" sa=\"4000000\" desc=\"\" />"
                   + "<cobertura id=\"06\" pma=\"\" ded=\"\" tipo=\"\" sa=\"800000\" desc=\"\" />"
                   + "<cobertura id=\"07\" pma=\"\" ded=\"\" tipo=\"\" sa=\"\" desc=\"\" />"
                   + "<cobertura id=\"08\" pma=\"\" ded=\"\" tipo=\"\" sa=\"\" desc=\"\" />"
                   + "<cobertura id=\"09\" pma=\"\" ded=\"\" tipo=\"\" sa=\"Auto Sustituto\" desc=\"\" />"
                   + "<cobertura id=\"10\" pma=\"\" ded=\"\" tipo=\"A\" sa=\"\" desc=\"\" />"
                   + "<cobertura id=\"12\" pma=\"\" ded=\"\" tipo=\"\" sa=\"\" desc=\"\" />"
                   + "<cobertura id=\"13\" pma=\"\" ded=\"\" tipo=\"\" sa=\"6\" desc=\"\" />"
                   + "<cobertura id=\"18\" pma=\"\" ded=\"\" tipo=\"\" sa=\"\" desc=\"\" />"
                   + "<cobertura id=\"23\" pma=\"\" ded=\"\" tipo=\"\" sa=\"1500000\" desc=\"\" />"
                   + "<cobertura id=\"25\" pma=\"\" ded=\"\" tipo=\"\" sa=\"2000000\" desc=\"\" />"
                   + "<cobertura id=\"26\" pma=\"\" ded=\"\" tipo=\"\" sa=\"2000000\" desc=\"\" />"
                   + "<cobertura id=\"33\" pma=\"\" ded=\"25\" tipo=\"\" sa=\"30000\" desc=\"\" />"
                   + "<cobertura id=\"34\" pma=\"\" ded=\"\" tipo=\"\" sa=\"4000000\" desc=\"\" />"
                   + "<cobertura id=\"35\" pma=\"\" ded=\"\" tipo=\"\" sa=\"\" desc=\"\" />"
                  + "</vehiculo><asegurado poblacion=\"\" estado=\"" + id_Ciudad + "\" id=\"\" tipopersona=\"\" pais=\"\" cp=\"\" colonia=\"\" numeroexterior=\"\" numerointerior=\"\" calle=\"\" materno=\"\" paterno=\"\" nombre=\"\" />"
                  + "<poliza id=\"\" tipo=\"A\" politicacancelacion=\"1\" beneficiario=\"\" tarifaderechos=\"1703\" tarifavalores=\"1703\" tarifacuotas=\"1703\" agente=\"07338\" formapago=\"C\" bonificacion=\"35\" moneda=\"0\" fecterminovig=\"" + DateTime.Now.AddYears(1).ToShortDateString() + "\" feciniciovig=\"" + DateTime.Now.ToShortDateString() + "\" fecemision=\"" + DateTime.Now.ToShortDateString() + "\" endoso=\"\" />"
                  + "<prima comision=\"\" primatotal=\"\" impuesto=\"\" recargo=\"\" derecho=\"\" primaneta=\"\" /><recibo serie=\"\" id=\"\" fecterminovig=\"\" feciniciovig=\"\" fecemision=\"\" comision=\"\" primatotal=\"\" impuesto=\"\" recargo=\"\" derecho=\"\" primaneta=\"\" numseriecertificado=\"\" anoaprobacion=\"\" numeroaprobacion=\"\" horaemision=\"\" folio=\"\" sellodigital=\"\" cadenaoriginal=\"\" />"
                  + "<error />"
                  + "</transaccion>"
                  + "</transacciones>";
            }
            else if (paquete == 1)
            {
                //root = Server.MapPath("~/Xml/AnaAmplia.xml");

                xmlAna = "<transacciones xmlns=\"\">"
                   + "<transaccion tiponegocio=\"\" negocio=\"640\" cotizacion=\"\" tipotransaccion=\"T\" version=\"1\">"
                   + "<vehiculo tipocargadescripcion=\"\" tipocarga=\"\" fecdispositivo=\"\" dispositivo=\"\" color=\"01\" poblacion=\"" + Rem + "\" estado=\"" + id_Ciudad + "\" conductorocupacion=\"\" conductorfecnac=\"\" conductorliciencia=\"\" conductor=\"\" placas=\"\" repuve=\"\" serie=\"\" motor=\"\" plan=\"1\" servicio=\"1\" uso=\"1\" descripcion=\"\" modelo=\"" + Modelo + "\" amis=\"" + AmisAna + "\" id=\"1\">"
                   + "<cobertura id=\"02\" pma=\"\" ded=\"5\" tipo=\"3\" sa=\"\" desc=\"\" />"
                   + "<cobertura id=\"04\" pma=\"\" ded=\"10\" tipo=\"3\" sa=\"\" desc=\"\" />"
                   + "<cobertura id=\"05\" pma=\"\" ded=\"\" tipo=\"\" sa=\"1000000\" desc=\"\" />"
                   + "<cobertura id=\"06\" pma=\"\" ded=\"\" tipo=\"\" sa=\"500000\" desc=\"\" />"
                   + "<cobertura id=\"07\" pma=\"\" ded=\"\" tipo=\"\" sa=\"\" desc=\"\" />"
                   //+ "<cobertura id=\"08\" pma=\"\" ded=\"\" tipo=\"\" sa=\"\" desc=\"\" />"              
                   + "<cobertura id=\"10\" pma=\"\" ded=\"\" tipo=\"B\" sa=\"\" desc=\"\" />"
                   + "<cobertura id=\"23\" pma=\"\" ded=\"\" tipo=\"\" sa=\"300000\" desc=\"\" />"
                   + "<cobertura id=\"25\" pma=\"\" ded=\"\" tipo=\"\" sa=\"500000\" desc=\"\" />"
                   + "<cobertura id=\"26\" pma=\"\" ded=\"\" tipo=\"\" sa=\"500000\" desc=\"\" />"
                   + "<cobertura id=\"34\" pma=\"\" ded=\"\" tipo=\"\" sa=\"2000000\" desc=\"\" />"
                   + "<cobertura id=\"35\" pma=\"\" ded=\"\" tipo=\"\" sa=\"\" desc=\"\" />"
                   + "</vehiculo><asegurado poblacion=\"\" estado=\"" + id_Ciudad + "\" id=\"\" tipopersona=\"\" pais=\"\" cp=\"\" colonia=\"\" numeroexterior=\"\" numerointerior=\"\" calle=\"\" materno=\"\" paterno=\"\" nombre=\"\" />"
                   + "<poliza id=\"\" tipo=\"A\" politicacancelacion=\"1\" beneficiario=\"\" tarifaderechos=\"1703\" tarifavalores=\"1703\" tarifacuotas=\"1703\" agente=\"07338\" formapago=\"T\" bonificacion=\"35\" moneda=\"0\" fecterminovig=\"" + DateTime.Now.AddYears(1).ToShortDateString() + "\" feciniciovig=\"" + DateTime.Now.ToShortDateString() + "\" fecemision=\"" + DateTime.Now.ToShortDateString() + "\" endoso=\"\" />"
                   + "<prima comision=\"\" primatotal=\"\" impuesto=\"\" recargo=\"\" derecho=\"\" primaneta=\"\" /><recibo serie=\"\" id=\"\" fecterminovig=\"\" feciniciovig=\"\" fecemision=\"\" comision=\"\" primatotal=\"\" impuesto=\"\" recargo=\"\" derecho=\"\" primaneta=\"\" numseriecertificado=\"\" anoaprobacion=\"\" numeroaprobacion=\"\" horaemision=\"\" folio=\"\" sellodigital=\"\" cadenaoriginal=\"\" />"
                   + "<error />"
                   + "</transaccion>"
                   + "</transacciones>";

            }
            else if (paquete == 2)
            {
                // root = Server.MapPath("~/Xml/AnaLimitada.xml");

                xmlAna = "<transacciones xmlns=\"\">"
                       + "<transaccion tiponegocio=\"\" negocio=\"640\" cotizacion=\"\" tipotransaccion=\"C\" version=\"3\">"
                       + "<vehiculo tipocargadescripcion=\"\" tipocarga=\"\" fecdispositivo=\"\" dispositivo=\"\" color=\"01\" poblacion=\"" + Rem + "\" estado=\"" + id_Ciudad + "\" conductorocupacion=\"\" conductorfecnac=\"\" conductorliciencia=\"\" conductor=\"\" placas=\"\" repuve=\"\" serie=\"\" motor=\"\" plan=\"3\" servicio=\"1\" uso=\"1\" descripcion=\"\" modelo=\"" + Modelo + "\" amis=\"" + AmisAna + "\" id=\"1\">"
                       + "<cobertura id=\"04\" pma=\"\" ded=\"5\" tipo=\"3\" sa=\"\" desc=\"\" />"
                       + "<cobertura id=\"06\" pma=\"\" ded=\"\" tipo=\"\" sa=\"500000\" desc=\"\" />"
                       + "<cobertura id=\"07\" pma=\"\" ded=\"\" tipo=\"\" sa=\"\" desc=\"\" />"
                       //  + "<cobertura id=\"08\" pma=\"\" ded=\"\" tipo=\"\" sa=\"\" desc=\"\" />"
                       + "<cobertura id=\"37\" pma=\"\" ded=\"\" tipo=\"\" sa=\"\" desc=\"\" />"
                       + "<cobertura id=\"10\" pma=\"\" ded=\"\" tipo=\"B\" sa=\"\" desc=\"\" />"
                       + "<cobertura id=\"23\" pma=\"\" ded=\"\" tipo=\"\" sa=\"300000\" desc=\"\" />"
                       + "<cobertura id=\"25\" pma=\"\" ded=\"\" tipo=\"\" sa=\"500000\" desc=\"\" />"
                       + "<cobertura id=\"26\" pma=\"\" ded=\"\" tipo=\"\" sa=\"500000\" desc=\"\" />"
                       + "<cobertura id=\"34\" pma=\"\" ded=\"\" tipo=\"\" sa=\"2000000\" desc=\"\" />"
                       + "</vehiculo><asegurado poblacion=\"\" estado=\"" + id_Ciudad + "\" id=\"\" tipopersona=\"\" pais=\"\" cp=\"\" colonia=\"\" numeroexterior=\"\" numerointerior=\"\" calle=\"\" materno=\"\" paterno=\"\" nombre=\"\" />"
                       + "<poliza id=\"\" tipo=\"A\" politicacancelacion=\"1\" beneficiario=\"\" tarifaderechos=\"1703\" tarifavalores=\"1703\" tarifacuotas=\"1703\" agente=\"07338\" formapago=\"C\" bonificacion=\"35\" moneda=\"0\" fecterminovig=\"" + DateTime.Now.AddYears(1).ToShortDateString() + "\" feciniciovig=\"" + DateTime.Now.ToShortDateString() + "\" fecemision=\"" + DateTime.Now.ToShortDateString() + "\" endoso=\"\" />"
                       + "<prima comision=\"\" primatotal=\"\" impuesto=\"\" recargo=\"\" derecho=\"\" primaneta=\"\" /><recibo serie=\"\" id=\"\" fecterminovig=\"\" feciniciovig=\"\" fecemision=\"\" comision=\"\" primatotal=\"\" impuesto=\"\" recargo=\"\" derecho=\"\" primaneta=\"\" numseriecertificado=\"\" anoaprobacion=\"\" numeroaprobacion=\"\" horaemision=\"\" folio=\"\" sellodigital=\"\" cadenaoriginal=\"\" />"
                       + "<error />"
                       + "</transaccion>"
                       + "</transacciones>";
            }
            else if (paquete == 3)
            {
                // root = Server.MapPath("~/Xml/AnaRC.xml");

                xmlAna = "<transacciones xmlns=\"\">"
                       + "<transaccion tiponegocio=\"\" negocio=\"640\" cotizacion=\"\" tipotransaccion=\"C\" version=\"1\">"
                       + "<vehiculo tipocargadescripcion=\"\" tipocarga=\"\" fecdispositivo=\"\" dispositivo=\"\" color=\"01\" poblacion=\"DISTRITO FEDERAL\" estado=\"01009\" conductorocupacion=\"\" conductorfecnac=\"\" conductorliciencia=\"\" conductor=\"\" placas=\"\" repuve=\"\" serie=\"\" motor=\"\" plan=\"4\" servicio=\"1\" uso=\"1\" descripcion=\"\" modelo=\"" + Modelo + "\" amis=\"" + AmisAna + "\" id=\"1\">"
                       // + "<cobertura id=\"02\" pma=\"\" ded=\"5\" tipo=\"1\" sa=\"\" desc=\"\" />" 
                       // + "<cobertura id=\"04\" pma=\"\" ded=\"10\" tipo=\"1\" sa=\"\" desc=\"\" />"
                       + "<cobertura id=\"06\" pma=\"\" ded=\"\" tipo=\"\" sa=\"1000000\" desc=\"\" />"
                       + "<cobertura id=\"07\" pma=\"\" ded=\"\" tipo=\"\" sa=\"\" desc=\"\" />"
                       + "<cobertura id=\"08\" pma=\"\" ded=\"\" tipo=\"\" sa=\"\" desc=\"\" /><cobertura id=\"09\" pma=\"\" ded=\"\" tipo=\"\" sa=\"Auto Sustituto\" desc=\"\" />"
                       + "<cobertura id=\"10\" pma=\"\" ded=\"\" tipo=\"B\" sa=\"\" desc=\"\" /><cobertura id=\"23\" pma=\"\" ded=\"\" tipo=\"\" sa=\"1500000\" desc=\"\" />"
                       + "<cobertura id=\"25\" pma=\"\" ded=\"\" tipo=\"\" sa=\"750000\" desc=\"\" /><cobertura id=\"26\" pma=\"\" ded=\"\" tipo=\"\" sa=\"750000\" desc=\"\" />"
                       + "<cobertura id=\"34\" pma=\"\" ded=\"\" tipo=\"\" sa=\"3000000\" desc=\"\" />"
                       + "</vehiculo><asegurado poblacion=\"\" estado=\"01001\" id=\"\" tipopersona=\"\" pais=\"\" cp=\"\" colonia=\"\" numeroexterior=\"\" numerointerior=\"\" calle=\"\" materno=\"\" paterno=\"\" nombre=\"\" />"
                       + "<poliza id=\"\" tipo=\"A\" politicacancelacion=\"1\" beneficiario=\"\" tarifaderechos=\"1505\" tarifavalores=\"1505\" tarifacuotas=\"1505\" agente=\"07338\" formapago=\"C\" bonificacion=\"0\" moneda=\"0\" fecterminovig=\"" + DateTime.Now.AddYears(1).ToShortDateString() + "\" feciniciovig=\"" + DateTime.Now.ToShortDateString() + "\" fecemision=\"" + DateTime.Now.ToShortDateString() + "\" endoso=\"\" />"
                       + "<prima comision=\"\" primatotal=\"\" impuesto=\"\" recargo=\"\" derecho=\"\" primaneta=\"\" /><recibo serie=\"\" id=\"\" fecterminovig=\"\" feciniciovig=\"\" fecemision=\"\" comision=\"\" primatotal=\"\" impuesto=\"\" recargo=\"\" derecho=\"\" primaneta=\"\" numseriecertificado=\"\" anoaprobacion=\"\" numeroaprobacion=\"\" horaemision=\"\" folio=\"\" sellodigital=\"\" cadenaoriginal=\"\" />"
                       + "<error />"
                       + "</transaccion>"
                       + "</transacciones>";
            }


            //doc.Load(root);
            //xmlAna = doc.InnerXml.ToString();
            WsAna.Service ws = new WsAna.Service();
            var tipo = WsAna.TipoTransaccion.Cotizacion;
            try
            {
                XmlDocument xm = new XmlDocument();
                xm.LoadXml(xmlAna);
                string xmlSalida = ws.Transaccion(xm, tipo, "07338", "K48Yp1Tv");
                XmlDocument XmlResp = new XmlDocument();
                XmlResp.LoadXml(xmlSalida);
                XmlNode cotizacion = XmlResp.SelectSingleNode("transacciones/transaccion");

                cot = cotizacion.OuterXml.Split('"')[5];
                this.CotAna.Text = cot.ToString();
                Session["NoCotAseg"] = cot.ToString();
                XmlNode nNombre = XmlResp.SelectSingleNode("transacciones/transaccion/prima");
                num = nNombre.OuterXml.Split('"');

            }

            catch (Exception x)
            {
                string error = x.ToString();
            }
            return num;

        }

        protected double CotizaAba(int paquete)
        {

            DataTable amis = Session["AmisAna"] as DataTable;

            string clave = amis.Rows[0]["claveChubb"].ToString();

            var proxy = new WsAba.ACCotizacionClient();

            // Creamos el objeto TOKEN en el que enviaremos las credenciales de usuario 
            // y asignamos los valores de usuario y contraseña
            var token = new WsAba.Token { usuario = "Pruebasan", password = "Seguro123*" };

            // Obtenemos el XML de salida
            string sRutaXml = Server.MapPath("~/Xml/XMLCotizacionABA.xml");
            double iPrimaAmpliaPlus;
            double iPrimaAmplia;
            var lo_Document = new XDocument();

            int iAntiguedad = DateTime.Now.Year - int.Parse("2012");


            lo_Document = XDocument.Load(sRutaXml);

            if (paquete == 0)
            {
                lo_Document.Descendants("PAQ").ToList()[0].Value = "1";
            }
            else if (paquete == 1)
            {
                lo_Document.Descendants("PAQ").ToList()[0].Value = "1";
            }
            else if (paquete == 2)
            {
                lo_Document.Descendants("PAQ").ToList()[0].Value = "2";
            }
            else if (paquete == 3)
            {
                lo_Document.Descendants("PAQ").ToList()[0].Value = "2";
            }
            lo_Document.Descendants("CVEVEH").ToList()[0].Value = clave;
            lo_Document.Descendants("MOD").ToList()[0].Value = "2012";
            lo_Document.Descendants("INIVIG").ToList()[0].Value = DateTime.Today.ToShortDateString().Substring(6, 4) + "-" + DateTime.Today.ToShortDateString().Substring(3, 2) + "-" + DateTime.Today.ToShortDateString().Substring(0, 2);
            lo_Document.Descendants("FINVIG").ToList()[0].Value = DateTime.Today.AddYears(1).ToShortDateString().Substring(6, 4) + "-" + DateTime.Today.ToShortDateString().Substring(3, 2) + "-" + DateTime.Today.ToShortDateString().Substring(0, 2);

            lo_Document.Descendants("EDO").ToList()[0].Value = "19";
            lo_Document.Descendants("MUN").ToList()[0].Value = "1037";

            lo_Document.Descendants("PD").ToList()[0].Value = "0";

            XDocument doc = new XDocument();
            XmlDocument xDoc = new XmlDocument();


            string sXmlSalida;
            string sXmlRespuesta;
            if (iAntiguedad <= 12)
            {
                //sXmlSalida = proxy.CotizaAuto(token, lo_Document.ToString());
                //sXmlRespuesta = (sXmlSalida);
                //doc = XDocument.Parse(sXmlRespuesta);
                //xDoc.LoadXml(sXmlRespuesta);
            }


            /************************************Cotizacion AmlpiaPlus************************************************/
            int estado = 8;
            double PrimaCotAba = 0;
            if (paquete == 0)
            {

                if (estado == 8)
                {
                    iPrimaAmpliaPlus = (from elementoAP in doc.Descendants("COB")
                                        let xElement = elementoAP.Element("COBID")
                                        where xElement != null && (xElement.Value.Equals("1") || xElement.Value.Equals("2")
                                                                || xElement.Value.Equals("3") || xElement.Value.Equals("84")//xElement.Value.Equals("50")
                                                                || xElement.Value.Equals("60") || xElement.Value.Equals("77")
                                                                || xElement.Value.Equals("4") || xElement.Value.Equals("61")
                                                                || xElement.Value.Equals("25") || xElement.Value.Equals("45")
                                                                || xElement.Value.Equals("66") || xElement.Value.Equals("53")
                                                                || xElement.Value.Equals("75") || xElement.Value.Equals("85")
                                                                || xElement.Value.Equals("49") || xElement.Value.Equals("393"))
                                        let element = elementoAP.Element("PTOTAL")
                                        where element != null
                                        select new { dPrecioTotal = double.Parse(element.Value) }).Sum(o => o.dPrecioTotal);

                }
                else
                {
                    iPrimaAmpliaPlus = (from elementoAP in doc.Descendants("COB")
                                        let xElement = elementoAP.Element("COBID")
                                        where xElement != null && (xElement.Value.Equals("1") || xElement.Value.Equals("2")
                                                                || xElement.Value.Equals("3") || xElement.Value.Equals("84")//xElement.Value.Equals("50")
                                                                || xElement.Value.Equals("60") || xElement.Value.Equals("77")
                                                                || xElement.Value.Equals("4") || xElement.Value.Equals("61")
                                                                || xElement.Value.Equals("25") || xElement.Value.Equals("45")
                                                                || xElement.Value.Equals("66") || xElement.Value.Equals("57")
                                                                || xElement.Value.Equals("75") || xElement.Value.Equals("85")
                                                                || xElement.Value.Equals("49") || xElement.Value.Equals("73"))
                                        let element = elementoAP.Element("PTOTAL")
                                        where element != null
                                        select new { dPrecioTotal = double.Parse(element.Value) }).Sum(o => o.dPrecioTotal);

                }
                return PrimaCotAba = iPrimaAmpliaPlus;
            }
            /*****************************cierra*******Cotizacion AmlpiaPlus************************************************/

            /************************************Cotizacion Amlpia************************************************/
            else if (paquete == 1)
            {
                if (estado == 8)
                {
                    iPrimaAmplia = (from elementoAP in doc.Descendants("COB")
                                    let xElement = elementoAP.Element("COBID")
                                    where xElement != null && (xElement.Value.Equals("1") || xElement.Value.Equals("2")
                                                            || xElement.Value.Equals("3") || xElement.Value.Equals("84")//xElement.Value.Equals("50")
                                                            || xElement.Value.Equals("60") || xElement.Value.Equals("77")
                                                            || xElement.Value.Equals("4") || xElement.Value.Equals("61")
                                                            || xElement.Value.Equals("66") || xElement.Value.Equals("53")
                                                            || xElement.Value.Equals("85"))
                                    let element = elementoAP.Element("PTOTAL")
                                    where element != null
                                    select new { dPrecioTotal = double.Parse(element.Value) }).Sum(o => o.dPrecioTotal);
                }
                else
                {
                    iPrimaAmplia = (from elementoAP in doc.Descendants("COB")
                                    let xElement = elementoAP.Element("COBID")
                                    where xElement != null && (xElement.Value.Equals("1") || xElement.Value.Equals("2")
                                                            || xElement.Value.Equals("3") || xElement.Value.Equals("84")//xElement.Value.Equals("50")
                                                            || xElement.Value.Equals("60") || xElement.Value.Equals("77")
                                                            || xElement.Value.Equals("4") || xElement.Value.Equals("61")
                                                            || xElement.Value.Equals("66") || xElement.Value.Equals("57")
                                                            || xElement.Value.Equals("85"))
                                    let element = elementoAP.Element("PTOTAL")
                                    where element != null
                                    select new { dPrecioTotal = double.Parse(element.Value) }).Sum(o => o.dPrecioTotal);
                }
                int subramo = 2;
                if (subramo == 2)
                {
                    iPrimaAmplia += (from elementoAP in doc.Descendants("COB")
                                     let xElement = elementoAP.Element("COBID")
                                     where xElement != null && (xElement.Value.Equals("49"))
                                     let element = elementoAP.Element("PTOTAL")
                                     where element != null
                                     select new { dPrecioTotal = double.Parse(element.Value) }).Sum(o => o.dPrecioTotal);
                }

                return PrimaCotAba = iPrimaAmplia;

            }
            /****************************cierra********Cotizacion Amlpia************************************************/


            /************************************Cotizacion Limitada************************************************/

            else if (paquete == 2)
            {
                if (estado == 8)
                {
                    iPrimaAmplia = (from elementoAP in doc.Descendants("COB")
                                    let xElement = elementoAP.Element("COBID")
                                    where xElement != null && (xElement.Value.Equals("2")
                                                            || xElement.Value.Equals("3") || xElement.Value.Equals("84")//xElement.Value.Equals("50")
                                                            || xElement.Value.Equals("60") || xElement.Value.Equals("77")
                                                            || xElement.Value.Equals("4") || xElement.Value.Equals("61")
                                                            || xElement.Value.Equals("66") || xElement.Value.Equals("53")
                                                            || xElement.Value.Equals("85"))
                                    let element = elementoAP.Element("PTOTAL")
                                    where element != null
                                    select new { dPrecioTotal = double.Parse(element.Value) }).Sum(o => o.dPrecioTotal);
                }
                else
                {
                    iPrimaAmplia = (from elementoAP in doc.Descendants("COB")
                                    let xElement = elementoAP.Element("COBID")
                                    where xElement != null && (xElement.Value.Equals("2")
                                                            || xElement.Value.Equals("3") || xElement.Value.Equals("84")//xElement.Value.Equals("50")
                                                            || xElement.Value.Equals("60") || xElement.Value.Equals("77")
                                                            || xElement.Value.Equals("4") || xElement.Value.Equals("61")
                                                            || xElement.Value.Equals("66") || xElement.Value.Equals("57")
                                                            || xElement.Value.Equals("85"))
                                    let element = elementoAP.Element("PTOTAL")
                                    where element != null
                                    select new { dPrecioTotal = double.Parse(element.Value) }).Sum(o => o.dPrecioTotal);
                }
                int subramo = 2;
                if (subramo == 2)
                {
                    iPrimaAmplia += (from elementoAP in doc.Descendants("COB")
                                     let xElement = elementoAP.Element("COBID")
                                     where xElement != null && (xElement.Value.Equals("49"))
                                     let element = elementoAP.Element("PTOTAL")
                                     where element != null
                                     select new { dPrecioTotal = double.Parse(element.Value) }).Sum(o => o.dPrecioTotal);
                }

                return PrimaCotAba = iPrimaAmplia;

            }
            /**************************cierra**********Cotizacion Limitada************************************************/

            /************************************Cotizacion RC************************************************/
            else if (paquete == 3)
            {
                if (estado == 8)
                {
                    iPrimaAmplia = (from elementoAP in doc.Descendants("COB")
                                    let xElement = elementoAP.Element("COBID")
                                    where xElement != null && (xElement.Value.Equals("3") || xElement.Value.Equals("84")//xElement.Value.Equals("50")
                                                            || xElement.Value.Equals("60") || xElement.Value.Equals("77")
                                                            || xElement.Value.Equals("4") || xElement.Value.Equals("61")
                                                            || xElement.Value.Equals("66") || xElement.Value.Equals("53")
                                                            || xElement.Value.Equals("85"))
                                    let element = elementoAP.Element("PTOTAL")
                                    where element != null
                                    select new { dPrecioTotal = double.Parse(element.Value) }).Sum(o => o.dPrecioTotal);
                }
                else
                {
                    iPrimaAmplia = (from elementoAP in doc.Descendants("COB")
                                    let xElement = elementoAP.Element("COBID")
                                    where xElement != null && (xElement.Value.Equals("3") || xElement.Value.Equals("84")//xElement.Value.Equals("50")
                                                            || xElement.Value.Equals("60") || xElement.Value.Equals("77")
                                                            || xElement.Value.Equals("4") || xElement.Value.Equals("61")
                                                            || xElement.Value.Equals("66") || xElement.Value.Equals("57")
                                                            || xElement.Value.Equals("85"))
                                    let element = elementoAP.Element("PTOTAL")
                                    where element != null
                                    select new { dPrecioTotal = double.Parse(element.Value) }).Sum(o => o.dPrecioTotal);
                }
                int subramo = 2;
                if (subramo == 2)
                {
                    iPrimaAmplia += (from elementoAP in doc.Descendants("COB")
                                     let xElement = elementoAP.Element("COBID")
                                     where xElement != null && (xElement.Value.Equals("49"))
                                     let element = elementoAP.Element("PTOTAL")
                                     where element != null
                                     select new { dPrecioTotal = double.Parse(element.Value) }).Sum(o => o.dPrecioTotal);
                }

                return PrimaCotAba = iPrimaAmplia;
            }

            /**************************cierra**********Cotizacion RC ************************************************/
            else
            {
                return 0.0;
            }
        }



        //protected void RadioButtonList1(object sender, EventArgs e)
        //{
        //    //string selectedValue = rblist1.SelectedValue;
        //    //Response.Write(selectedValue);
        //}

        string remplazar(string texto)
        {
            string con = "áàäéèëíìïóòöúùuñÁÀÄÉÈËÍÌÏÓÒÖÚÙÜÑçÇ";
            string sin = "aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcC";
            for (int i = 0; i < con.Length; i++)
            {
                texto = texto.Replace(con[i], sin[i]);
            }
            return texto;
        }

        protected void EnviaCorreoCOtizacion()
        {
            CreaPDF();
            MailMessage mail = new MailMessage();          
            string body = CuerpoCorreoCotizacion();
            mail.From = new MailAddress("drackster55@hotmail.com");
            mail.To.Add("drackster55@hotmail.com");
            mail.Subject = "Estrategia en protección cotización seguro de auto";
            mail.Attachments.Add(new Attachment(Server.MapPath("/CoticacionPDF/") + Session["NoCotAseg"].ToString() + ".pdf"));
            mail.Body = body;
            mail.IsBodyHtml = true;
            SmtpClient smtpServer = new SmtpClient("smtp.live.com"); ;
            smtpServer.Port = 587;
            smtpServer.Credentials = new System.Net.NetworkCredential("drackster55@hotmail.com", "suadero28") as ICredentialsByHost;
            smtpServer.EnableSsl = true;
            ServicePointManager.ServerCertificateValidationCallback =
                delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
                { return true; };
            try
            {
                smtpServer.Send(mail);
                Response.Write("<script>alert('Se ha enviado una copia a su correo')</script>");                      
            }
            catch (Exception e)
            {
                
                Response.Write("<script>alert('"+ e.Message.ToString()+"')</script>");
            }

            HyperLink HyperLink1 = new HyperLink();
            var pdfFile = ConfigurationManager.AppSettings["rutaCotizacion"].ToString() + Session["NoCotAseg"].ToString() + ".pdf"; //or VaribleCustmer.pdf
            HyperLink1.NavigateUrl = String.Format("~/customerUnit/{0}", pdfFile);
        }

        protected string CuerpoCorreoCotizacion()
        {

            string paquete = Session["TipoCotizacion"].ToString() == "" ? "Amplia EP" : Session["TipoCotizacion"].ToString();
            Session["Paquete"] = paquete;
            //"Da click " + "<a href=" + url + ">aquí</a>" + " para descargar tu reconocimiento VRMedia";
            string body = "<html>"
                            + "<body>"//Server.MapPath("/Images/logo_arriba_final.jpg")
                            + "<img src=" + Server.MapPath("/Images/logo_arriba_final.jpg") + ">"
                            + "<br />"
                            + "<label>" + "Cotizacion Estrategia en protección" + "</label>"
                            + "<br />"
                            + "<label>" + "Cotizacion número: " + Session["NoCotizacion"].ToString() + "</label>"
                            + "<br />"
                            + "<br />"
                            + "<label>" + "Datos del vehículo:" + "</label>"
                            + "<br />"
                            + "<label>" + "Marca:" + Session["Marca"].ToString() + "</label>"
                            + "<br />"
                            + "<label>" + "Submarca:" + Session["Submarca"].ToString() + "</label>"
                            + "<br />"
                            + "<label>" + "Descripcion:" + Session["Descripcion"].ToString() + "</label>"
                            + "<br />"
                            + "<label>" + "Modelo:" + Session["Modelo"].ToString() + "</label>"
                            + "<br />"
                            + "<br />"
                            + "<label>" + "Paquete: " + paquete + "</label>"
                            + "</body>"
                          + "</html>";
            // , " + Session["Submarca"].ToString()+", "+ Session["Descripcion"].ToString()+ ", "+"Modelo: "+ Session["Modelo"].ToString()+
            return body;
        }

        protected void CreaPDF()
        {
            Document doc = new Document(PageSize.LETTER);
            // Indicamos donde vamos a guardar el documento     Session["NoCotizacion"].ToString()
            PdfWriter writer = PdfWriter.GetInstance(doc, new FileStream(Server.MapPath("/CoticacionPDF/") + Session["NoCotAseg"].ToString() + ".pdf", FileMode.Create));

            // Abrimos el archivo
            doc.Open();

            // Creamos el tipo de Font que vamos utilizar
            iTextSharp.text.Font _standardFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 8, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);

            doc.Add(new Paragraph("Fecha de elaboración:" + " " + DateTime.Now.ToShortDateString()));

            iTextSharp.text.Image imagen = iTextSharp.text.Image.GetInstance(Server.MapPath("/Images/") +"logo_arriba_final.jpg");
            imagen.BorderWidth = 0;

            imagen.SetAbsolutePosition(400, 720);


            float percentage = 0.0f;
            percentage = 200 / imagen.Width;
            imagen.ScalePercent(percentage * 100);

            // Insertamos la imagen en el documento
            doc.Add(imagen);
            // Escribimos el encabezamiento en el documento
            doc.Add(new Paragraph("No.cotizacion: " + " " + Session["NoCotizacion"].ToString()));
            doc.Add(Chunk.NEWLINE);

            // Creamos una tabla que contendrá el nombre, apellido y país
            // de nuestros visitante.
            PdfPTable tblTitulo = new PdfPTable(1);
            tblTitulo.HorizontalAlignment = 0;


            // Configuramos el título de las columnas de la tabla
            PdfPCell clNombre = new PdfPCell(new Phrase("Nombre: " + Session["Nombre"].ToString() + " " + Session["Apellido"].ToString(), _standardFont));
            clNombre.BorderWidth = 0;
            // Añadimos las celdas a la tabla
            tblTitulo.AddCell(clNombre);
            doc.Add(tblTitulo);

            // datos vehículo
            PdfPTable tblDtosAuto = new PdfPTable(1);
            tblDtosAuto.HorizontalAlignment = 0;
            PdfPCell vehiculo = new PdfPCell(new Phrase("Vehículo: " + Session["Marca"].ToString() + " " + Session["Submarca"].ToString() + " " + Session["Modelo"].ToString() + " " + Session["Descripcion"].ToString(), _standardFont));
            vehiculo.BorderWidth = 0;
            tblDtosAuto.AddCell(vehiculo);
            doc.Add(tblDtosAuto);


            // datos estado

            PdfPTable tblLugarCot = new PdfPTable(1);
            tblLugarCot.HorizontalAlignment = 0;
            PdfPCell Estado = new PdfPCell(new Phrase("Lugar: " + Session["Estado"].ToString() + " " + Session["Municipio"].ToString() + " " + Session["Colonias"].ToString() + " C.P. " + Session["CodigoPostal"].ToString(), _standardFont));
            Estado.BorderWidth = 0;
            tblLugarCot.AddCell(Estado);
            doc.Add(tblLugarCot);



            //tipo de cotización
            PdfPTable tbltipoCobertura = new PdfPTable(1);
            tbltipoCobertura.HorizontalAlignment = 0;
            string tipoCob = Session["TipoCotizacion"].ToString() == "" ? "Amplia EP" : Session["TipoCotizacion"].ToString();
            PdfPCell descCob = new PdfPCell(new Phrase("Cobertura: " + tipoCob, _standardFont));
            descCob.BorderWidth = 0;
            tbltipoCobertura.AddCell(descCob);
            doc.Add(tbltipoCobertura);

            PdfPTable TbllAseguradora = new PdfPTable(1);
            TbllAseguradora.HorizontalAlignment = 0;
            PdfPCell NombreAseg = new PdfPCell(new Phrase("Asegurdora: " + "ANA Seguros", _standardFont));
            NombreAseg.BorderWidth = 0;
            TbllAseguradora.AddCell(NombreAseg);            
            doc.Add(TbllAseguradora);
            doc.Add(Chunk.NEWLINE);

            /*empieza formas de pago*/
            PdfPTable pagosYprecios = new PdfPTable(2);
            pagosYprecios.WidthPercentage = 75;

            PdfPCell pago = new PdfPCell(new Phrase("Forma de pago", _standardFont));
            pago.BorderWidth = 0;
            pago.BorderWidthBottom = 1f;

            PdfPCell precio = new PdfPCell(new Phrase("Precio", _standardFont));
            precio.BorderWidth = 0;
            precio.BorderWidthBottom = 1f;

            pagosYprecios.AddCell(pago);
            pagosYprecios.AddCell(precio);
            #region Contado
            pago = new PdfPCell(new Phrase("Contado", _standardFont));
            pago.BorderWidth = 0;
            pago.BackgroundColor = BaseColor.YELLOW;
            precio = new PdfPCell(new Phrase(Session["PrecioContado"].ToString(), _standardFont));
            precio.BorderWidth = 0;
            precio.BackgroundColor = BaseColor.YELLOW;

            pagosYprecios.AddCell(pago);
            pagosYprecios.AddCell(precio);
            #endregion

            #region precioSemestral
            pago = new PdfPCell(new Phrase("Semestral", _standardFont));
            pago.BorderWidth = 0;
            pago.BackgroundColor = BaseColor.YELLOW;
            precio = new PdfPCell(new Phrase(Session["PrecioSemestral"].ToString(), _standardFont));
            precio.BorderWidth = 0;
            precio.BackgroundColor = BaseColor.YELLOW;
            pagosYprecios.AddCell(pago);
            pagosYprecios.AddCell(precio); // 

            pago = new PdfPCell(new Phrase("Primer pago", _standardFont));
            pago.BorderWidth = 0;
            precio = new PdfPCell(new Phrase(Session["PrecioSem1"].ToString(), _standardFont));
            precio.BorderWidth = 0;
            pagosYprecios.AddCell(pago);
            pagosYprecios.AddCell(precio);

            pago = new PdfPCell(new Phrase("Subsecuentes", _standardFont));
            pago.BorderWidth = 0;
            precio = new PdfPCell(new Phrase(Session["PrecioSemSub"].ToString(), _standardFont));
            precio.BorderWidth = 0;
            pagosYprecios.AddCell(pago);
            pagosYprecios.AddCell(precio);
            #endregion

            #region Trimestral
            pago = new PdfPCell(new Phrase("Trimestral", _standardFont));
            pago.BorderWidth = 0;
            pago.BackgroundColor = BaseColor.YELLOW;
            precio = new PdfPCell(new Phrase(Session["PrecioTrimestral"].ToString(), _standardFont));
            precio.BorderWidth = 0;
            precio.BackgroundColor = BaseColor.YELLOW;
            pagosYprecios.AddCell(pago);
            pagosYprecios.AddCell(precio);

            pago = new PdfPCell(new Phrase("Primer pago", _standardFont));
            pago.BorderWidth = 0;
            precio = new PdfPCell(new Phrase(Session["PrecioTri1"].ToString(), _standardFont));
            precio.BorderWidth = 0;
            pagosYprecios.AddCell(pago);
            pagosYprecios.AddCell(precio);

            pago = new PdfPCell(new Phrase("Subsecuentes", _standardFont));
            pago.BorderWidth = 0;
            precio = new PdfPCell(new Phrase(Session["PrecioTriSub"].ToString(), _standardFont));
            precio.BorderWidth = 0;
            pagosYprecios.AddCell(pago);
            pagosYprecios.AddCell(precio);
            #endregion

            #region Mensual
            pago = new PdfPCell(new Phrase("Mensual", _standardFont));
            pago.BorderWidth = 0;
            pago.BackgroundColor = BaseColor.YELLOW;
            precio = new PdfPCell(new Phrase(Session["PrecioMensual"].ToString(), _standardFont));
            precio.BackgroundColor = BaseColor.YELLOW;
            precio.BorderWidth = 0;
            pagosYprecios.AddCell(pago);
            pagosYprecios.AddCell(precio);

            pago = new PdfPCell(new Phrase("Primer pago", _standardFont));
            pago.BorderWidth = 0;
            precio = new PdfPCell(new Phrase(Session["PrecioMen1"].ToString(), _standardFont));
            precio.BorderWidth = 0;
            pagosYprecios.AddCell(pago);
            pagosYprecios.AddCell(precio);

            pago = new PdfPCell(new Phrase("Subsecuentes", _standardFont));
            pago.BorderWidth = 0;
            precio = new PdfPCell(new Phrase(Session["PrecioMenSub"].ToString(), _standardFont));
            precio.BorderWidth = 0;
            pagosYprecios.AddCell(pago);
            pagosYprecios.AddCell(precio);
            #endregion
            doc.Add(pagosYprecios);

            doc.Add(Chunk.NEWLINE);

            #region agrega coberturas
            // datos coberturas y aseguradoras
            PdfPTable TblCobYAseg = new PdfPTable(3);
            TblCobYAseg.WidthPercentage = 75;

            PdfPCell cobertura = new PdfPCell(new Phrase("Cobertura", _standardFont));
            cobertura.BorderWidth = 0;
            cobertura.BorderWidthBottom = 0.75f;

            PdfPCell SumaAsegurada = new PdfPCell(new Phrase("Suma asegurada", _standardFont));
            SumaAsegurada.BorderWidth = 0;
            SumaAsegurada.BorderWidthBottom = 0.75f;

            PdfPCell Deducible = new PdfPCell(new Phrase("Deducible", _standardFont));
            Deducible.BorderWidth = 0;
            Deducible.BorderWidthBottom = 0.75f;

            TblCobYAseg.AddCell(cobertura);
            TblCobYAseg.AddCell(SumaAsegurada);
            TblCobYAseg.AddCell(Deducible);

            // daños materiales ********************************************************

            if (tipoCob == "Amplia EP")
            {
                var Datoscoberturas = AccesoDatos.FnObtenerCoberturas(1, 0);

                foreach (DataRow datos in Datoscoberturas.Rows)
                {
                    cobertura = new PdfPCell(new Phrase(datos.ItemArray[0].ToString(), _standardFont));
                    cobertura.BorderWidth = 0;

                    SumaAsegurada = new PdfPCell(new Phrase(datos.ItemArray[1].ToString(), _standardFont));
                    SumaAsegurada.BorderWidth = 0;

                    Deducible = new PdfPCell(new Phrase(datos.ItemArray[2].ToString(), _standardFont));
                    Deducible.BorderWidth = 0;

                    TblCobYAseg.AddCell(cobertura);
                    TblCobYAseg.AddCell(SumaAsegurada);
                    TblCobYAseg.AddCell(Deducible);

                }
                doc.Add(TblCobYAseg);
            }
            else if (tipoCob == "Amplia")
            {
                var Datoscoberturas = AccesoDatos.FnObtenerCoberturas(1, 1);

                foreach (DataRow datos in Datoscoberturas.Rows)
                {
                    cobertura = new PdfPCell(new Phrase(datos.ItemArray[0].ToString(), _standardFont));
                    cobertura.BorderWidth = 0;

                    SumaAsegurada = new PdfPCell(new Phrase(datos.ItemArray[1].ToString(), _standardFont));
                    SumaAsegurada.BorderWidth = 0;

                    Deducible = new PdfPCell(new Phrase(datos.ItemArray[2].ToString(), _standardFont));
                    Deducible.BorderWidth = 0;

                    TblCobYAseg.AddCell(cobertura);
                    TblCobYAseg.AddCell(SumaAsegurada);
                    TblCobYAseg.AddCell(Deducible);

                }
                doc.Add(TblCobYAseg);
            }
            else
            {
                var Datoscoberturas = AccesoDatos.FnObtenerCoberturas(1, 2);

                foreach (DataRow datos in Datoscoberturas.Rows)
                {
                    cobertura = new PdfPCell(new Phrase(datos.ItemArray[0].ToString(), _standardFont));
                    cobertura.BorderWidth = 0;

                    SumaAsegurada = new PdfPCell(new Phrase(datos.ItemArray[1].ToString(), _standardFont));
                    SumaAsegurada.BorderWidth = 0;

                    Deducible = new PdfPCell(new Phrase(datos.ItemArray[2].ToString(), _standardFont));
                    Deducible.BorderWidth = 0;

                    TblCobYAseg.AddCell(cobertura);
                    TblCobYAseg.AddCell(SumaAsegurada);
                    TblCobYAseg.AddCell(Deducible);

                }
                doc.Add(TblCobYAseg);
            }

            #endregion
            doc.Add(Chunk.NEWLINE);

            doc.Add(new Paragraph("Esta cotización tiene una vigencia hasta el día:" + " " + DateTime.Now.AddDays(7).ToShortDateString()));
            doc.Add(Chunk.NEWLINE);
            doc.Add(new Paragraph("Teiéfono: " + " 5555-4444"));

            doc.Close();
            writer.Close();

           



        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            
            Session["NoCotAseg"] = "123456";
            if (File.Exists(ConfigurationManager.AppSettings["rutaCotizacion"].ToString() + Session["NoCotAseg"].ToString() + ".pdf"))
            {
                OpenPdf();
                return;
            }

            EnviaCorreoCOtizacion();
            OpenPdf();
        }

        protected void OpenPdf()
        {

            string FilePath = Server.MapPath("/CoticacionPDF/") + Session["NoCotAseg"].ToString() + ".pdf";
            WebClient User = new WebClient();
            Byte[] FileBuffer = User.DownloadData(FilePath);

            if (FileBuffer != null)

            {
                Response.ContentType = "application/pdf";
                Response.AppendHeader("content-disposition", "attachment; filename=" + (new FileInfo(FilePath)).Name);
                //Response.AddHeader("content-length", FileBuffer.Length.ToString());
                Response.BinaryWrite(FileBuffer);
                Response.End();
            }

        }
    }
}