﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace CotizadorAutos
{
    public partial class Cotizacion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
                lblMarca.Text = Session["Marca"].ToString();
                lblsubmarca.Text = Session["Submarca"].ToString();
                lblaño.Text = Session["Modelo"].ToString();
                lblVersion.Text = Session["Descripcion"].ToString();
                lblCotizacion1.Text = Session["NoCotizacion"].ToString();

                fnObtenerAseguradoras(1);
        }

        private DataSet fnLlenaGridCoberturas(int paquete)
        {
            var Coberturas = new DataSet();
            var conexion = new SqlConnection();
            conexion.ConnectionString = ConfigurationManager.ConnectionStrings["ConexionWindows"].ConnectionString;
            {
                conexion.Open();
                using (SqlDataAdapter da = new SqlDataAdapter("spObtenerPaquetes" + " " + paquete, conexion))
                {
                    da.Fill(Coberturas);
                    conexion.Close();
                }
            }
            return Coberturas;             
        }

       

        public string[] fnObtenerAseguradoras (int idVehiculoHomologado)
        {
            string[] array = { "1", "2", "3", "4", "5", "6" };
            cotiza(array, 0);
            return array;
        }

        public void cotiza (string[] Aseguradoras, int iPaquete)
        {

            string primas = "";
            

            foreach(string Aseg in Aseguradoras)
            {

               

                switch(Aseg)
                {
                    case"1":
                        {
                            DataTable PrimasNetas = new DataTable();
                            PrimasNetas = AccesoDatos.fnObtenerPrimas();
                            primas = PrimasNetas.Rows[0]["Prima"].ToString();                            
                            ImgAseg1.ImageUrl = "Images/axa.jpg";

                            /*Contado*/
                            double recargo=1.05;                            
                            double DerechoPoliza = 400;
                            double contado = Convert.ToDouble(primas.Split('|')[iPaquete].ToString());                       
                            double result = (contado + DerechoPoliza) * 1.16;
                            lbContadoAseg1.Text = result.ToString("C");


                             /*Semestral*/
                            contado = Convert.ToDouble(primas.Split('|')[iPaquete].ToString());
                            double Semestral;
                            Semestral = (((contado + DerechoPoliza) * recargo)*1.16);
                            double SemP1 = ((((contado/2) + DerechoPoliza) * recargo) * 1.16);
                            double SemSub = (((contado / 2)  * recargo) * 1.16);
                            lbSemestralAseg1.Text = Semestral.ToString("C");
                            lbSemestralAseg1P1.Text = SemP1.ToString("C");
                            lbSemestralAseg1Sub.Text = SemSub.ToString("C"); 
                        }
                        break;

                    case "2":
                        {
                            DataTable PrimasNetas = new DataTable();
                            PrimasNetas = AccesoDatos.fnObtenerPrimas();
                            primas = PrimasNetas.Rows[0]["Prima"].ToString();
                            primas.Split('|');
                            ImgAseg2.ImageUrl = "Images/aba.jpg";

                            /*Contado*/
                            double recargo = 1.05;
                            double DerechoPoliza = 400;
                            double contado = Convert.ToDouble(primas.Split('|')[iPaquete].ToString());
                            double result = (contado + DerechoPoliza) * 1.16;
                            lbContadoAseg2.Text = result.ToString("C");

                            /*Semestral*/
                            contado = Convert.ToDouble(primas.Split('|')[iPaquete].ToString());
                            double Semestral;
                            Semestral = (((contado + DerechoPoliza) * recargo) * 1.16);
                            double SemP1 = ((((contado / 2) + DerechoPoliza) * recargo) * 1.16);
                            double SemSub = (((contado / 2) * recargo) * 1.16);
                            lbSemestralAseg2.Text = Semestral.ToString("C");
                            lbSemestralAseg2P1.Text = SemP1.ToString("C");
                            lbSemestralAseg2Sub.Text = SemSub.ToString("C"); 
                        }
                        break;

                    case "3":
                        {
                            DataTable PrimasNetas = new DataTable();
                            PrimasNetas = AccesoDatos.fnObtenerPrimas();
                            primas = PrimasNetas.Rows[0]["Prima"].ToString();
                            primas.Split('|');
                            ImgAseg3.ImageUrl = "Images/zurich.jpg";

                            /*Contado*/
                            double recargo = 1.05;
                            double DerechoPoliza = 400;
                            double contado = Convert.ToDouble(primas.Split('|')[iPaquete].ToString());
                            double result = (contado + DerechoPoliza) * 1.16;
                            lbContadoAseg3.Text = result.ToString("C");

                            /*Semestral*/
                            contado = Convert.ToDouble(primas.Split('|')[iPaquete].ToString());
                            double Semestral;
                            Semestral = (((contado + DerechoPoliza) * recargo) * 1.16);
                            double SemP1 = ((((contado / 2) + DerechoPoliza) * recargo) * 1.16);
                            double SemSub = (((contado / 2) * recargo) * 1.16);
                            lbSemestralAseg3.Text = Semestral.ToString("C");
                            lbSemestralAseg3P1.Text = SemP1.ToString("C");
                            lbSemestralAseg3Sub.Text = SemSub.ToString("C"); 
                        }
                        break;

                    case "4":
                        {
                            DataTable PrimasNetas = new DataTable();
                            PrimasNetas = AccesoDatos.fnObtenerPrimas();
                            primas = PrimasNetas.Rows[0]["Prima"].ToString();
                            primas.Split('|');
                            ImgAseg4.ImageUrl = "Images/mapfre.jpg";

                            /*Contado*/
                            double recargo = 1.05;
                            double DerechoPoliza = 400;
                            double contado = Convert.ToDouble(primas.Split('|')[iPaquete].ToString());
                            double result = (contado + DerechoPoliza) * 1.16;
                            lbContadoAseg4.Text = result.ToString("C");

                            /*Semestral*/
                            contado = Convert.ToDouble(primas.Split('|')[iPaquete].ToString());
                            double Semestral;
                            Semestral = (((contado + DerechoPoliza) * recargo) * 1.16);
                            double SemP1 = ((((contado / 2) + DerechoPoliza) * recargo) * 1.16);
                            double SemSub = (((contado / 2) * recargo) * 1.16);
                            lbSemestralAseg4.Text = Semestral.ToString("C");
                            lbSemestralAseg4P1.Text = SemP1.ToString("C");
                            lbSemestralAseg4Sub.Text = SemSub.ToString("C"); 

                        }
                        break;

                    case "5":
                        {
                            DataTable PrimasNetas = new DataTable();
                            PrimasNetas = AccesoDatos.fnObtenerPrimas();
                            primas = PrimasNetas.Rows[0]["Prima"].ToString();
                            primas.Split('|');
                            ImgAseg5.ImageUrl = "Images/zurich.jpg";

                            /*Contado*/
                            double recargo = 1.05;
                            double DerechoPoliza = 400;
                            double contado = Convert.ToDouble(primas.Split('|')[iPaquete].ToString());
                            double result = (contado + DerechoPoliza) * 1.16;
                            lbContadoAseg5.Text = result.ToString("C");

                            /*Semestral*/
                            contado = Convert.ToDouble(primas.Split('|')[iPaquete].ToString());
                            double Semestral;
                            Semestral = (((contado + DerechoPoliza) * recargo) * 1.16);
                            double SemP1 = ((((contado / 2) + DerechoPoliza) * recargo) * 1.16);
                            double SemSub = (((contado / 2) * recargo) * 1.16);
                            lbSemestralAseg5.Text = Semestral.ToString("C");
                            lbSemestralAseg5P1.Text = SemP1.ToString("C");
                            lbSemestralAseg5Sub.Text = SemSub.ToString("C"); 
                        }
                        break;

                    case "6":
                        {
                            DataTable PrimasNetas = new DataTable();
                            PrimasNetas = AccesoDatos.fnObtenerPrimas();
                            primas = PrimasNetas.Rows[0]["Prima"].ToString();
                            primas.Split('|');
                            ImgAseg6.ImageUrl = "Images/rsa.jpg";


                            /*Contado*/
                            double recargo = 1.05;
                            double DerechoPoliza = 400;
                            double contado = Convert.ToDouble(primas.Split('|')[iPaquete].ToString());
                            double result = (contado + DerechoPoliza) * 1.16;
                            lbContadoAseg6.Text = result.ToString("C");

                            /*Semestral*/
                            contado = Convert.ToDouble(primas.Split('|')[iPaquete].ToString());
                            double Semestral;
                            Semestral = (((contado + DerechoPoliza) * recargo) * 1.16);
                            double SemP1 = ((((contado / 2) + DerechoPoliza) * recargo) * 1.16);
                            double SemSub = (((contado / 2) * recargo) * 1.16);
                            lbSemestralAseg6.Text = Semestral.ToString("C");
                            lbSemestralAseg6P1.Text = SemP1.ToString("C");
                            lbSemestralAseg6Sub.Text = SemSub.ToString("C"); 
                        }
                        break;

                }  
                            

            }

        }

        protected void btnAmpliaEP_Click(object sender, EventArgs e)
        {
            string[] array = { "1", "2", "3", "4", "5", "6" };
            cotiza(array, 0);

            var coberturas = fnLlenaGridCoberturas(1);
            gwCoberturas.DataSource = coberturas;
            gwCoberturas.DataBind();   
        }

        protected void btnAmplia_Click(object sender, EventArgs e)
        {
            string[] array = { "1", "2", "3", "4", "5", "6" };
            cotiza(array, 1);

            var coberturas = fnLlenaGridCoberturas(2);
            gwCoberturas.DataSource = coberturas;
            gwCoberturas.DataBind();   

        }

        protected void btnLimitadaEP_Click(object sender, EventArgs e)
        {
            string[] array = { "1", "2", "3", "4", "5", "6" };
            cotiza(array, 2);

            var coberturas = fnLlenaGridCoberturas(3);
            gwCoberturas.DataSource = coberturas;
            gwCoberturas.DataBind();   
        }

        protected void btnLimitada_Click(object sender, EventArgs e)
        {
            string[] array = { "1", "2", "3", "4", "5", "6" };
            cotiza(array, 3);


            var coberturas = fnLlenaGridCoberturas(4);
            gwCoberturas.DataSource = coberturas;
            gwCoberturas.DataBind();   
        }

       

       
    }
}