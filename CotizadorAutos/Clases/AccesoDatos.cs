﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace CotizadorAutos
{
    public class AccesoDatos
    {    
        public static DataTable fnObtenerPrimas()
        {         
            var Primas = new DataTable();
            var conexion = new SqlConnection();
            conexion.ConnectionString = ConfigurationManager.ConnectionStrings["ConexionWindows"].ConnectionString;
            {
                conexion.Open();
                using (SqlDataAdapter da = new SqlDataAdapter("spObtenerPrimaAba", conexion))
                {
                    da.Fill(Primas);
                    conexion.Close();
                }
            }
            return Primas;
        }
        public static DataSet fnObtenerMarcas(int subramo)
        {

            var dtMarcas = new DataSet();
            var conexion = new SqlConnection();
            conexion.ConnectionString = ConfigurationManager.ConnectionStrings["ConexionWindows"].ConnectionString;
            {
                conexion.Open();
                using (SqlDataAdapter da = new SqlDataAdapter("spObtenerMarcaPorSubramo " + subramo, conexion))
                {
                    da.Fill(dtMarcas);
                    conexion.Close();
                }


            }
            return dtMarcas;
        }
        public static DataTable fnObtenerSubmarca(int idMarca)
        {
            var dtSubMarcas = new DataTable();
            var conexion = new SqlConnection();
            conexion.ConnectionString = ConfigurationManager.ConnectionStrings["ConexionWindows"].ConnectionString;
            {
                conexion.Open();
                using (SqlDataAdapter da = new SqlDataAdapter("spObtenerSubmarca" + " " + idMarca + "", conexion))
                {
                    da.Fill(dtSubMarcas);
                    conexion.Close();
                }
            }
            return dtSubMarcas;
        }
        public static DataTable fnObtenerAño(int submarca)
        {
            var iAño = new DataTable();
            var conexion = new SqlConnection();
            conexion.ConnectionString = ConfigurationManager.ConnectionStrings["ConexionWindows"].ConnectionString;
            {
                conexion.Open();
                using (SqlDataAdapter da = new SqlDataAdapter("spObtenerModeloPorSubmarca" + " " + submarca + "", conexion))
                {
                    da.Fill(iAño);
                    conexion.Close();
                }
            }
            return iAño;
        }
        public static DataTable fnObtenerDescripcion(int iModeloSubmarca)
        {
            var Descripcion = new DataTable();           
            var conexion = new SqlConnection();
            conexion.ConnectionString = ConfigurationManager.ConnectionStrings["ConexionWindows"].ConnectionString;
            {
                conexion.Open();
                using (SqlDataAdapter da = new SqlDataAdapter("spObtenerDescripcion" + " " + iModeloSubmarca, conexion))
                {
                    da.Fill(Descripcion);
                    conexion.Close();
                }
            }
            return Descripcion;
        }
        public static DataTable fnObtenerEstados()
        {
            var Descripcion = new DataTable();
            var conexion = new SqlConnection();
            conexion.ConnectionString = ConfigurationManager.ConnectionStrings["ConexionWindows"].ConnectionString;
            {
                conexion.Open();
                using (SqlDataAdapter da = new SqlDataAdapter("spObtenerEstados", conexion))
                {
                    da.Fill(Descripcion);
                    conexion.Close();
                }
            }
            return Descripcion;
        }
        public static DataTable fnObtenerMunicipios(int iEstado)
        {
            var Descripcion = new DataTable();
            var conexion = new SqlConnection();
            conexion.ConnectionString = ConfigurationManager.ConnectionStrings["ConexionWindows"].ConnectionString;
            {
                conexion.Open();
                using (SqlDataAdapter da = new SqlDataAdapter("spObtenerMunicipios " + iEstado + "", conexion))
                {
                    da.Fill(Descripcion);
                    conexion.Close();
                }
            }
            return Descripcion;
        }
        public static DataTable fnObtenerColonias(int iMunicipio)
        {
            var Colonias = new DataTable();
            var conexion = new SqlConnection();
            conexion.ConnectionString = ConfigurationManager.ConnectionStrings["ConexionWindows"].ConnectionString;
            {
                conexion.Open();
                using (SqlDataAdapter da = new SqlDataAdapter("spObtenerColonias " + iMunicipio, conexion))
                {
                    da.Fill(Colonias);
                    conexion.Close();
                }
            }
            return Colonias;

        }
        public static DataSet fnObtenerCodigosPostales(int idCodigoPostal)
        {
            var CodigosPostales = new DataSet();
            var conexion = new SqlConnection();
            conexion.ConnectionString = ConfigurationManager.ConnectionStrings["ConexionWindows"].ConnectionString;
            {
                conexion.Open();
                using (SqlDataAdapter da = new SqlDataAdapter("spObtenerCodigoPostal " + idCodigoPostal, conexion))
                {
                    da.Fill(CodigosPostales);
                    conexion.Close();
                }
            }
            return CodigosPostales;
        }
        public static DataSet fnObtenerColoniaMunicipioEstado(string CodigoPostal)
        {
            var dsUbicacion = new DataSet();
            var conexion = new SqlConnection();
            conexion.ConnectionString = ConfigurationManager.ConnectionStrings["ConexionWindows"].ConnectionString;
            {
                conexion.Open();
                using (SqlDataAdapter da = new SqlDataAdapter("spObtenerUbicacionCP " + "'" + CodigoPostal + "'", conexion))
                {
                    da.Fill(dsUbicacion);
                    conexion.Close();
                }
            }
            return dsUbicacion;
        }
        public static DataSet fnObtenerNumeroDeCotizacion()
        {
            var NoCotizacion = new DataSet();
            var conexion = new SqlConnection();
            conexion.ConnectionString = ConfigurationManager.ConnectionStrings["ConexionWindows"].ConnectionString;
            {
                conexion.Open();
                using (SqlDataAdapter da = new SqlDataAdapter("SELECT MAX(idCotizacion) as idCotizacion FROM tCotizaciones", conexion))
                {
                    da.Fill(NoCotizacion);
                    conexion.Close();
                }
            }
            return NoCotizacion;
        }
        public static void fnIsertaDatosliente(
                                           int idUsuario,
                                        string Nombre,
                                        string Apellido,
                                        string Estado,
                                        string Municipio,
                                        string Colonia,
                                        string CodigoPostal,
                                        string Telefono,
                                        string CorreoElectronico,
                                        string Marca,
                                        string Submarca,
                                        string Modelo,
                                        string descripcion)
        {
            var conexion = new SqlConnection();
            conexion.ConnectionString = ConfigurationManager.ConnectionStrings["ConexionWindows"].ConnectionString;
            conexion.Open();
            using (SqlCommand insert = new SqlCommand("spInsertaDatosCotizacion " + idUsuario + ",'" + Nombre + "'," + "'" + Apellido + "'," + "'" + Estado + "'," + "'" + Municipio + "'," + "'" + Colonia + "'," + "'" + CodigoPostal + "'," + "'" + Telefono + "'," + "'" + CorreoElectronico + "','"
                                                                                  + Marca + "'" + ",'" + Submarca + "'" + ",'" + Modelo + "'" + ",'" + descripcion + "'", conexion))
            {
                insert.ExecuteReader();
                conexion.Close();
            }       
        
        } 

    }
}
