﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace CotizadorAutos
{
    public partial class ReporteCotizaciones : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblNombre.Text = Session["NombreAgente"].ToString();
            lblID.Text = Session["idUsuario"].ToString();

            DataSet cotizaciones = AccesoDatos.fnObtenerCotizacionesPorUsuario(int.Parse(Session["idUsuario"].ToString()));

            gwCotizaciones.DataSource = cotizaciones;
            gwCotizaciones.DataBind();
        }
    }
}