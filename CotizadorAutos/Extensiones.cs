﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace CotizadorAutos
{
    public static class Extensiones
    {
        public static void LlenaLista(this DropDownList pCombo,System.Data.DataTable pFuenteDatos, 
            string pValue, 
            string pTextField)
        {
            pCombo.DataSource = pFuenteDatos;
            pCombo.DataTextField = pTextField;
            pCombo.DataValueField = pValue;
            pCombo.DataBind();

            pCombo.Items.Insert(0, new ListItem("Elija una Opcion..", "0"));
        }

    }
}