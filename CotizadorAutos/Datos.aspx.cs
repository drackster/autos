﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Security;

namespace CotizadorAutos
{
    public partial class Datos : System.Web.UI.Page
    {

        protected void Page_Init(object sender, EventArgs e)
        {

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Session["Usuario"] = "Cliente";

            MaintainScrollPositionOnPostBack = true;
            if(!IsPostBack)
            {
                var conexion = new SqlConnection();
                var dt = fnObtenerMarcas();
                ddlMarca.DataSource = dt;
                ddlMarca.DataMember = dt.Tables[0].ToString();
                ddlMarca.DataTextField = "sMarca";
                ddlMarca.DataValueField = "idMarcaSubramo";
                ddlMarca.DataBind();
                ddlMarca.Items.Insert(0, new ListItem("Elija una Opcion..", "0"));


                var dtEstados = fnObtenerEstados();
                    ddlEstado.DataSource = dtEstados;
                    ddlEstado.DataTextField = dtEstados.Columns[1].ToString();
                    ddlEstado.DataValueField = dtEstados.Columns[0].ToString();
                    ddlEstado.DataBind();
                    ddlEstado.Items.Insert(0, new ListItem("Elija una Opcion..", "0"));

                    var dsEdad = fnLlenaComboEdad();
                    ddlEstado.DataSource = dsEdad;
                    ddlEdad.DataSource = dsEdad;
                    ddlEdad.DataTextField = "iEdad";
                    //ddlEdad.DataValueField = dtEstados.Columns[0].ToString();
                    ddlEdad.DataBind();
                    ddlEdad.Items.Insert(0, new ListItem("Elija su edad", "0"));
                

            }

        }      
       
        protected void ddlMarca_SelectedIndexChanged(object sender, EventArgs e)
        {

            ddlSubmarca.Items.Clear();
            ddlaño.Items.Clear();
            ddldescripcion.Items.Clear();

            var iMarca = int.Parse(ddlMarca.SelectedValue);
            var conexion = new SqlConnection();
            var dt = fnObtenerSubmarca(iMarca);

            ddlSubmarca.DataSource = dt;
            ddlSubmarca.DataTextField = dt.Columns[1].ToString();
            ddlSubmarca.DataValueField = dt.Columns[0].ToString();
            ddlSubmarca.DataBind();
            ddlSubmarca.Items.Insert(0, new ListItem("Elija una Opcion..", "0"));

            ddlSubmarca.Enabled = true;
        }

        protected void ddlSubmarca_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlaño.Items.Clear();
            ddldescripcion.Items.Clear();
            var iSubmarca = int.Parse(ddlSubmarca.SelectedValue);
            var conexion = new SqlConnection();
            var dt = fnObtenerAño(iSubmarca);

            ddlaño.DataSource = dt;
            ddlaño.DataTextField = dt.Columns[1].ToString();
            ddlaño.DataValueField = dt.Columns[0].ToString();
            ddlaño.DataBind();
            ddlaño.Items.Insert(0, new ListItem("Elija una Opcion..", "0"));

            ddlaño.Enabled = true;
        
        }

        protected void ddlaño_SelectedIndexChanged(object sender, EventArgs e)
        {
           
            var conexion = new SqlConnection();
            int iModeloSubmarca = int.Parse(ddlaño.SelectedValue);
            var dt = fnObtenerDescripcion(iModeloSubmarca);

            ddldescripcion.DataSource = dt;
            ddldescripcion.DataTextField = dt.Columns[1].ToString();
            ddldescripcion.DataValueField = dt.Columns[0].ToString();
            ddldescripcion.DataBind();
            ddldescripcion.Items.Insert(0, new ListItem("Elija una Opcion..", "0"));

            ddldescripcion.Enabled = true;
        }

        protected void ddldescripcion_SelectedIndexChanged(object sender, EventArgs e)
        {
            //var conexion = new SqlConnection();
            //var dt = fnObtenerEstados();
            //ddlEstado.DataSource = dt;
            //ddlEstado.DataTextField = dt.Columns[1].ToString();            
            //ddlEstado.DataBind();

            //ddlEstado.Enabled = true;
        }

        protected void ddlEstado_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlMunicipio.Items.Clear();
            ddlColonias.Items.Clear();
            txtCP.Text = string.Empty;

            var iEstado = int.Parse(ddlEstado.SelectedValue.ToString());         
            var dt = fnObtenerMunicipios(iEstado);
            ddlMunicipio.DataSource = dt;
            ddlMunicipio.DataTextField = dt.Columns[1].ToString();
            ddlMunicipio.DataValueField = dt.Columns[0].ToString();
            ddlMunicipio.DataBind();
            ddlMunicipio.Items.Insert(0, new ListItem("Elija una Opcion..", "0"));

            ddlMunicipio.Enabled = true;

        }

        protected void ddlMunicipio_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlColonias.Items.Clear();
            txtCP.Text = string.Empty;
            var idMunicipio = int.Parse(ddlMunicipio.SelectedValue);
            var dt = fnObtenerColonias(idMunicipio);
            ddlColonias.DataSource = dt;
            ddlColonias.DataTextField = dt.Columns[1].ToString();
            ddlColonias.DataValueField = dt.Columns[0].ToString();
            ddlColonias.DataBind();
            ddlColonias.Items.Insert(0, new ListItem("Elija una Opcion..", "0"));
            ddlColonias.Enabled = true;

        }

        protected void ddlColonias_SelectedIndexChanged(object sender, EventArgs e)
        {
            var iCodigosPostales = int.Parse(ddlColonias.SelectedValue);
            var dt = fnObtenerCodigosPostales(iCodigosPostales);
            txtCP.Text = dt.Tables[0].Rows[0]["sCodigoPostal"].ToString();
           
        }

        protected void txtCP_TextChanged(object sender, EventArgs e)
        {
            if (txtCP.Text != string.Empty)
            {
                divColonia.Visible = true;
                divMunicipio.Visible = true;
                divEstado.Visible = true;

                ddlEstado.Items.Clear();
                ddlMunicipio.Items.Clear();
                ddlColonias.Items.Clear();
                DataSet dtCP = fnObtenerColoniaMunicipioEstado(txtCP.Text);

                var tabla0 = dtCP.Tables[0];
                var tabla1 = dtCP.Tables[1];
                var tabla2 = dtCP.Tables[2];

                ddlEstado.DataSource = tabla2;
                ddlEstado.DataTextField = "sEstado";
                ddlEstado.DataValueField = "idEstado";
                ddlEstado.DataBind();

                ddlMunicipio.DataSource = tabla1;
                ddlMunicipio.DataTextField = "sMunicipio";
                ddlMunicipio.DataValueField = "idMunicipio";
                ddlMunicipio.DataBind();

                ddlColonias.DataSource = tabla0;
                ddlColonias.DataTextField = "sColonia";
                ddlColonias.DataValueField = "idColonia";
                ddlColonias.DataBind();
            }

            else
            {
                ddlEstado.Items.Clear();
                ddlMunicipio.Items.Clear();
                ddlColonias.Items.Clear();

                var dtEstados = fnObtenerEstados();
                ddlEstado.DataSource = dtEstados;
                ddlEstado.DataTextField = dtEstados.Columns[1].ToString();
                ddlEstado.DataValueField = dtEstados.Columns[0].ToString();
                ddlEstado.DataBind();
                ddlEstado.Items.Insert(0, new ListItem("Elija una Opcion..", "0"));
            }
        }

        private DataSet fnObtenerMarcas()
        {
            int subramo;
            if(rbAutos.Checked)
            {
                subramo = 1;
            }
            else
            {
                subramo = 2;
            }
            
            var dtMarcas = new DataSet();
            dtMarcas = AccesoDatos.fnObtenerMarcas(subramo);
            return dtMarcas;
        }

        private DataTable fnObtenerSubmarca(int idMarca)
        {
            var dtSubMarcas = new DataTable();
            dtSubMarcas = AccesoDatos.fnObtenerSubmarca(idMarca);
            return dtSubMarcas;
        }

        private DataTable fnObtenerAño(int submarca)
        {
            var iAño = new DataTable();
            iAño = AccesoDatos.fnObtenerAño(submarca);
            return iAño;
        }

        private DataTable fnObtenerDescripcion(int iModeloSubmarca)
        {
            var Descripcion = new DataTable();
            Descripcion = AccesoDatos.fnObtenerDescripcion(iModeloSubmarca);   
            return Descripcion;
        }

        private DataTable fnObtenerEstados()
        {
            var Descripcion = new DataTable();
            Descripcion = AccesoDatos.fnObtenerEstados();
            return Descripcion;
        }

        private DataTable fnObtenerMunicipios(int iEstado)
        {
            var Descripcion = new DataTable();
            Descripcion = AccesoDatos.fnObtenerMunicipios(iEstado);
            return Descripcion;
        }   
        
        private DataTable fnObtenerColonias (int iMunicipio)
        {
            var Colonias = new DataTable();
            Colonias = AccesoDatos.fnObtenerColonias(iMunicipio);
            return Colonias;
        }

        private DataSet fnObtenerCodigosPostales(int idCodigoPostal)
        {
            var CodigosPostales = new DataSet();
            CodigosPostales = AccesoDatos.fnObtenerCodigosPostales(idCodigoPostal);
            return CodigosPostales;
        }

        private DataSet fnObtenerColoniaMunicipioEstado(string CodigoPostal)
        {
            var dsUbicacion = new DataSet();
            dsUbicacion = AccesoDatos.fnObtenerColoniaMunicipioEstado(CodigoPostal);
            return dsUbicacion;        
        }

        private DataSet fnLlenaComboEdad()
        {
            var Edades = new DataSet();
            var conexion = new SqlConnection();
            conexion.ConnectionString = ConfigurationManager.ConnectionStrings["ConexionWindows"].ConnectionString;
            {
                conexion.Open();
                using (SqlDataAdapter da = new SqlDataAdapter("select * from tedad", conexion))
                {
                    da.Fill(Edades);
                    conexion.Close();
                }
            }
            return Edades;

        }

        private DataSet fnObtenerNumeroDeCotizacion()
        {
            var NoCotizacion = new DataSet();
            NoCotizacion = AccesoDatos.fnObtenerNumeroDeCotizacion();
            return NoCotizacion;             
        }

        private void fnIsertaDatosliente(
                                           int idUsuario,
                                        string Nombre,
                                        string Apellido,
                                        string Estado,
                                        string Municipio,
                                        string Colonia,
                                        string CodigoPostal,
                                        string Telefono,
                                        string CorreoElectronico,
                                        string Marca,
                                        string Submarca,
                                        string Modelo,
                                        string descripcion)
        {

            AccesoDatos.fnIsertaDatosliente(idUsuario, Nombre, Apellido, Estado, Municipio, Colonia, CodigoPostal, Telefono, CorreoElectronico, Marca,
                                                 Submarca, Modelo, descripcion);      
        }

        


         protected void FnCargaddl(DropDownList combo,
                              DataTable dtTabla,
                              string sComboCampoTexto,
                              string sComboCampoValue,
                              string sPrimerItemComboTexto,
                              string sPrimerItemComboValue)
    {

        combo.SelectedIndex = -1;

        if (dtTabla.Rows.Count > 0)
        {
            combo.DataSource = dtTabla;
            combo.DataTextField = dtTabla.Columns[sComboCampoTexto].ColumnName;
            combo.DataValueField = dtTabla.Columns[sComboCampoValue].ColumnName;
            combo.DataBind();
        }
        if (sPrimerItemComboTexto.Length > 0)
            combo.Items.Insert(0, new ListItem(sPrimerItemComboTexto, sPrimerItemComboValue));

        combo.SelectedIndex = 0;

    }

         protected void ImgCotiza_Click(object sender, ImageClickEventArgs e)
         {
             Session.Add("Nombre", txtNombre.Text.ToUpper());
             Session.Add("Apellido", txtApellido.Text.ToUpper());
             Session.Add("Estado", ddlEstado.SelectedItem.Text.TrimEnd());
             Session.Add("Municipio", ddlMunicipio.SelectedItem.Text);
             Session.Add("Colonias", ddlColonias.SelectedItem.Text);
             Session.Add("CodigoPostal", txtCP.Text);
             Session.Add("Telefono", txtTelefono.Text);
             Session.Add("CorreoElectronico", txtCorreo.Text.ToLower());
             Session.Add("Marca", ddlMarca.SelectedItem.Text);
             Session.Add("Submarca", ddlSubmarca.SelectedItem.Text);
             Session.Add("Modelo", ddlaño.SelectedItem.Text);
             Session.Add("Descripcion", ddldescripcion.SelectedItem.Text);
             try
             {
                 fnIsertaDatosliente(
                                     0,//id usuarui 0 = cliente
                                     Session["Nombre"].ToString(),
                                     Session["Apellido"].ToString(),
                                     Session["Estado"].ToString(),
                                     Session["Municipio"].ToString(),
                                     Session["Colonias"].ToString(),
                                     Session["CodigoPostal"].ToString(),
                                     Session["Telefono"].ToString(),
                                     Session["CorreoElectronico"].ToString(),
                                     Session["Marca"].ToString(),
                                     Session["Submarca"].ToString(),
                                     Session["Modelo"].ToString(),
                                     Session["Descripcion"].ToString()
                                    );

                 var Cotizacion = fnObtenerNumeroDeCotizacion();
                 Session.Add("NoCotizacion", Cotizacion.Tables[0].Rows[0]["idCotizacion"]);
             }

             catch(Exception x)
             {
                 string display = x.Message;
                 ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + display + "');", true);
             }
             Response.Redirect("Cotizacion.aspx");
         }

         protected void txtTelefono_TextChanged(object sender, EventArgs e)
         {
             
         }

        

        



      

      
    }
}